//
//  MIDIEntity.h
//  PianoTutorMac
//
//  Created by MAG on 01/03/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDI.h"

@interface MIDIEntity : NSObject

-(id)initWithEntity:(MIDIEntityRef)entity;

@end
