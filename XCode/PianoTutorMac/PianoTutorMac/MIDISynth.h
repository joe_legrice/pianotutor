//
//  UnityMIDISynth.h
//  PianoTutorMac
//
//  Created by MAG on 04/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIVirtualDevice.h"

@interface MIDISynth : NSObject <MIDIDestination> {
    
}

-(void)handleOutputPackets:(const MIDIPacketList*)packetList;

@end