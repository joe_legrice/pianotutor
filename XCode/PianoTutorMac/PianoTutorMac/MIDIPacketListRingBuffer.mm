//
//  RingBuffer.m
//  PianoTutorMac
//
//  Created by MAG on 10/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIPacketListRingBuffer.h"
#include <string>

@interface MIDIPacketListRingBuffer ()

@property (atomic, assign) int capacity;
@property (atomic, assign) int currentPopIndex;
@property (atomic, assign) int currentPushIndex;
//@property (atomic, assign) MIDIPacketList[] buffer;

@end

@implementation MIDIPacketListRingBuffer : NSObject

typedef const MIDIPacketList* MIDIPacketListConstPtr;
MIDIPacketListConstPtr* buffer;

const int c_defaultSize = 25;

-(id)init
{
    return [self initWithSize:c_defaultSize];
}

-(id)initWithSize:(int)size
{
    if (self = [super init])
    {
        self.capacity = size;
        self.currentPopIndex = 0;
        self.currentPushIndex = 0;
        buffer = new MIDIPacketListConstPtr[size];
//        memset();
    }
    return self;
}

-(BOOL)hasNext
{
    return self.currentPopIndex != self.currentPushIndex;
}

-(void)pushObject:(const MIDIPacketList*)object
{
    buffer[self.currentPushIndex] = object;
    
    int newPushIndex = [self getNextIndex:self.currentPushIndex];
    if (newPushIndex == self.currentPopIndex)
    {
        // We have gone full-circle, we should expand the ring buffer.
        for (int expandIndex = 0; expandIndex < self.capacity; expandIndex++)
        {
//            [self.buffer addObject:[NSNull null]];
        }
        
        self.capacity *= 2;
        newPushIndex = [self getNextIndex:self.currentPushIndex];
    }
    self.currentPushIndex = newPushIndex;
}

-(MIDIPacketList*)popObject
{
    if ([self hasNext])
    {
        MIDIPacketList* result = (MIDIPacketList*)buffer[self.currentPopIndex];
        self.currentPopIndex = [self getNextIndex:self.currentPopIndex];
        return result;
    }
    else
    {
        // Array Empty
        return nil;
    }
}

-(int)getNextIndex:(int)afterIndex
{
    return (afterIndex + 1) < self.capacity ? afterIndex + 1 : 0;
}

@end
