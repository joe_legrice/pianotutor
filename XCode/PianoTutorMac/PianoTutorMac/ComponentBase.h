//
//  ComponentBase.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 21/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ComponentBase : NSObject

@property (nonatomic, assign) int bus;
@property (nonatomic, readonly) AUNode node;
@property (nonatomic, readonly) AudioUnit unit;

-(id)initWithType:(OSType)componentType andSubtype:(OSType)componentSubType;
-(void)attachToGraph:(AUGraph)graph;
-(Float64)outputSampleRate;
-(BOOL)attachInputCallback:(AURenderCallback)renderCallback toBus:(UInt32)busNumber;
-(BOOL)attachRenderCallback:(AURenderCallback)renderCallback toBus:(UInt32)busNumber inGraph:(AUGraph)graph;

@end