//
//  MIDIVirtualDevice.h
//  PianoTutorMac
//
//  Created by MAG on 29/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDI.h"

@interface MIDIVirtualDevice : NSObject <MIDIDevice>

-(id)initWithSource:(id<MIDISource>)virtualInput andDestination:(id<MIDIDestination>)virtualDestination;
-(void)closeVirtualSource;
-(void)closeVirtualDestination;

@end