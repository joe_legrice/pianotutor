//
//  DefaultOutput.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 21/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "DefaultOutput.h"

@interface DefaultOutput()

@end

@implementation DefaultOutput

-(id)init
{
    if (self = [super initWithType:kAudioUnitType_Output andSubtype:kAudioUnitSubType_DefaultOutput])
    {
    }
    return self;
}

@end
