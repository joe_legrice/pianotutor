//
//  TestDestinationOutput.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 07/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIVirtualDevice.h"

@interface TestDestinationOutput : NSObject <MIDIDestination>

-(void)handleOutputPackets:(const MIDIPacketList*)packetList;

@end