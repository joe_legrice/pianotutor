//
//  main.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 30/01/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIPhysicalDeviceManager.h"
#import "MIDIPhysicalDevice.h"
#import "MIDIVirtualDevice.h"
#import "TestSourceInput.h"
#import "TestDestinationOutput.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        MIDIPhysicalDeviceManager* dm = [[MIDIPhysicalDeviceManager alloc] init];
        for (MIDIPhysicalDevice* pd in dm.allDevices)
        {
            NSLog(@"%@", [pd getProperties]);
        }
//        TestSourceInput* tsi = [[TestSourceInput alloc] init];
//        MIDIVirtualDevice* virtualInput = [[MIDIVirtualDevice alloc] initWithSource:tsi
//                                                                         andDestination:nil];
//        
//        TestDestinationOutput* tdi = [[TestDestinationOutput alloc] init];
//        MIDIVirtualDevice* virtualOutput = [[MIDIVirtualDevice alloc] initWithSource:nil
//                                                                          andDestination:tdi];
//        
//        MIDI* m = [[MIDI alloc] init];
//        [m connectDeviceSource:virtualInput];
//        [m connectDeviceDestination:virtualOutput];
//        
//        [tsi sendRandomNote];
//        [NSThread sleepForTimeInterval:0.5];
//        [tsi sendRandomNote];
//        [NSThread sleepForTimeInterval:0.5];
//        [tsi sendRandomNote];
//        [NSThread sleepForTimeInterval:0.5];
    }
    return 0;
}
