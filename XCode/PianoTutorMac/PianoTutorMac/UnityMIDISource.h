//
//  UnityMIDIIn.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 27/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIVirtualDevice.h"

@interface UnityMIDISource : NSObject <MIDISource>

@property (nonatomic, assign) MIDIEndpointRef sourceEndpoint;

-(void)didReceiveData:(Byte*)data;

@end
