//
//  UnityMIDI.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 31/01/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMIDI/CoreMIDI.h>

@protocol MIDIDevice <NSObject>

-(void)attachSourceToMIDIConnection:(MIDIClientRef*)midiClient inputPort:(MIDIPortRef)inputPort;
-(MIDIEndpointRef*)attachDestinationToMIDIConnection:(MIDIClientRef*)midiClient;

@end

@protocol MIDISource <NSObject>

@property (nonatomic, assign) MIDIEndpointRef sourceEndpoint;

@end

@protocol MIDIDestination <NSObject>

-(void)handleOutputPackets:(const MIDIPacketList*)packetList;

@end

@interface MIDI : NSObject

@property (nonatomic, readonly) MIDIClientRef midiClient;
@property (nonatomic, readonly) MIDIPortRef inputPort;
@property (nonatomic, readonly) MIDIPortRef outputPort;

-(void)connectDeviceSource:(id<MIDIDevice>)device;
-(void)connectDeviceDestination:(id<MIDIDevice>)device;

@end
