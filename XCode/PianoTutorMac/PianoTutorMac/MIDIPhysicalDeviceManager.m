//
//  MIDIPhysicalDeviceManager.m
//  PianoTutorMac
//
//  Created by MAG on 02/03/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIPhysicalDevice.h"
#import "MIDIPhysicalDeviceManager.h"

@interface MIDIPhysicalDeviceManager ()

@property (nonatomic, strong) NSArray* allDevices;

@end

@implementation MIDIPhysicalDeviceManager

-(id)init
{
    if (self= [super init])
    {
        [self refreshDeviceList];
    }
    return self;
}

-(void)refreshDeviceList
{
    NSMutableArray* devices = [NSMutableArray array];
    
    // Get Internal Devices
    uint numberOfDevices = (uint)MIDIGetNumberOfDevices();
    for (int deviceIndex=0; deviceIndex < numberOfDevices; deviceIndex++)
    {
        MIDIDeviceRef deviceRef = MIDIGetDevice(deviceIndex);
        [devices addObject:[[MIDIPhysicalDevice alloc] initWithDevice:deviceRef]];
    }
    
    // Get External Devices
    uint numberOfExternalDevices = (uint)MIDIGetNumberOfExternalDevices();
    for (int deviceIndex=0; deviceIndex < numberOfExternalDevices; deviceIndex++)
    {
        MIDIDeviceRef deviceRef = MIDIGetExternalDevice(deviceIndex);
        [devices addObject:[[MIDIPhysicalDevice alloc] initWithDevice:deviceRef]];
    }
    
    self.allDevices = [NSArray arrayWithArray:devices];
}

@end
