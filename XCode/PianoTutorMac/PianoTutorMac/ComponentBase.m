//
//  ComponentBase.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 21/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "ComponentBase.h"

@interface ComponentBase()

@property (nonatomic, assign) AUNode node;
@property (nonatomic, assign) AudioUnit unit;
@property (nonatomic, assign) AudioComponentDescription componentDescription;

@end

@implementation ComponentBase

-(id)initWithType:(OSType)componentType andSubtype:(OSType)componentSubType
{
    if (self = [super init])
    {
        AudioComponentDescription temp = {
            .componentType = componentType,
            .componentSubType = componentSubType,
            .componentManufacturer = kAudioUnitManufacturer_Apple,
            .componentFlags = 0,
            .componentFlagsMask = 0
        };
        self.componentDescription = temp;
    }
    return self;
}

-(Float64)outputSampleRate
{
    Float64 sampleRate;
    
    UInt32 size = sizeof(sampleRate);
    OSStatus result = AudioUnitGetProperty(self.unit,
                                           kAudioUnitProperty_SampleRate,
                                           kAudioUnitScope_Output,
                                           0,
                                           &sampleRate,
                                           &size);
    return sampleRate;
}

-(void)attachToGraph:(AUGraph)graph {
    AUNode tempNode;
    OSStatus addNodeResult = AUGraphAddNode(graph, &_componentDescription, &tempNode);
    self.node = tempNode;
    
    AudioUnit temp;
    OSStatus getUnitResult = AUGraphNodeInfo (graph, self.node, NULL, &temp);
    self.unit = temp;
}


-(BOOL)attachInputCallback:(AURenderCallback)renderCallback toBus:(UInt32)busNumber
{
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = renderCallback;
    callbackStruct.inputProcRefCon  = (__bridge void*)self;
    OSStatus result = AudioUnitSetProperty(self.unit,
                                           kAudioUnitProperty_SetRenderCallback,
                                           kAudioUnitScope_Global,
                                           busNumber,
                                           &callbackStruct,
                                           sizeof(callbackStruct));
    return result;
}

-(BOOL)attachRenderCallback:(AURenderCallback)renderCallback toBus:(UInt32)busNumber inGraph:(AUGraph)graph
{
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = renderCallback;
    callbackStruct.inputProcRefCon  = (__bridge void*)self;
    OSStatus result = AUGraphSetNodeInputCallback(graph,
                                                  self.node,
                                                  busNumber,
                                                  &callbackStruct);
    return result;
}


@end
