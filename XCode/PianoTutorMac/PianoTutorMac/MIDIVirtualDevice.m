//
//  MIDIVirtualDevice.m
//  PianoTutorMac
//
//  Created by MAG on 29/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIVirtualDevice.h"

@interface MIDIVirtualDevice ()

@property (nonatomic, assign) MIDIPortRef inputPort;
@property (nonatomic, assign) MIDIEndpointRef virtualSourceEndpoint;
@property (nonatomic, assign) MIDIEndpointRef virtualDestinationEndpoint;

@property (nonatomic, strong) id<MIDISource> virtualSourceInput;
@property (nonatomic, strong) id<MIDIDestination> virtualDestinationOutput;

@end

@implementation MIDIVirtualDevice

-(id)initWithSource:(id<MIDISource>)virtualInput andDestination:(id<MIDIDestination>)virtualDestination
{
    if (self = [super init])
    {
        self.virtualSourceInput = virtualInput;
        self.virtualDestinationOutput = virtualDestination;
    }
    return self;
}

-(void)attachSourceToMIDIConnection:(MIDIClientRef*)midiClient inputPort:(MIDIPortRef)inputPort
{
    if (self.virtualSourceEndpoint == 0 && self.virtualSourceInput != nil)
    {
        MIDISourceCreate(*midiClient, CFSTR("UnityMIDI Virtual Source"), &_virtualSourceEndpoint);
        [self assignUniqueIDForVirtualEndpoint: self.virtualSourceEndpoint
                                 withReference:@"UnityMIDI Virtual Source"];
        MIDIPortConnectSource(inputPort, self.virtualSourceEndpoint, (__bridge void*)self);
        self.virtualSourceInput.sourceEndpoint = self.virtualSourceEndpoint;
        
        self.inputPort = inputPort;
    }
}

-(MIDIEndpointRef*)attachDestinationToMIDIConnection:(MIDIClientRef*)midiClient
{
    if (self.virtualDestinationEndpoint == 0 && self.virtualDestinationOutput != nil)
    {
        MIDIDestinationCreate(*midiClient, CFSTR("UnityMIDI Virtual Destination"),
                              MIDIVirtualOutputRead, (__bridge void*)self, &_virtualDestinationEndpoint);
        [self assignUniqueIDForVirtualEndpoint:self.virtualDestinationEndpoint
                                 withReference:@"UnityMIDI Virtual Destination"];
        return &_virtualDestinationEndpoint;
    }
    else
    {
        return NULL;
    }
}

-(void)closeVirtualSource
{
    if (self.virtualSourceEndpoint != 0)
    {
        MIDIPortDisconnectSource(self.inputPort, self.virtualSourceEndpoint);
        MIDIEndpointDispose(self.virtualSourceEndpoint);
    }
}

-(void)closeVirtualDestination
{
    if (self.virtualDestinationEndpoint != 0)
    {
        MIDIEndpointDispose(self.virtualDestinationEndpoint);
    }
}

-(void)assignUniqueIDForVirtualEndpoint:(MIDIEndpointRef)endpoint withReference:(NSString*)reference
{
    SInt32 uniqueID = (SInt32)[[NSUserDefaults standardUserDefaults] integerForKey:reference];
    if (uniqueID)
    {
        OSStatus s = MIDIObjectSetIntegerProperty(endpoint, kMIDIPropertyUniqueID, uniqueID);
        if (s == kMIDIIDNotUnique)
        {
            uniqueID = 0;
        }
    }
    
    if (!uniqueID)
    {
        OSStatus s = MIDIObjectGetIntegerProperty(endpoint, kMIDIPropertyUniqueID, &uniqueID);
        if (s == noErr)
        {
            [[NSUserDefaults standardUserDefaults] setInteger:uniqueID forKey:reference];
        }
    }
}

void MIDIVirtualOutputRead(const MIDIPacketList* pktlist, void* readProcRefCon, void* srcConnRefCon)
{
    NSLog(@"MIDI Virtual Output Read");
    MIDIVirtualDevice* self = (__bridge MIDIVirtualDevice*)readProcRefCon   ;
    [self.virtualDestinationOutput handleOutputPackets:pktlist];
}

@end
