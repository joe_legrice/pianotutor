//
//  MIDIPhysicalDeviceManager.h
//  PianoTutorMac
//
//  Created by MAG on 02/03/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDI.h"

@interface MIDIPhysicalDeviceManager : NSObject

@property (nonatomic, readonly) NSArray* allDevices;

-(void)refreshDeviceList;

@end