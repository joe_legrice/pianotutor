//
//  Header.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 27/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (*UnityMIDIMessageHook)(Byte* data);

extern "C"
{
    void InitialiseUnityMIDI(UnityMIDIMessageHook destinationHook);
    void SendMIDI(Byte* midiData);
    const char* GetDevicePropertiesJSON();
}

@interface UnityMIDI : NSObject

@end
