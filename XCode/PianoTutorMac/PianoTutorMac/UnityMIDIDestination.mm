//
//  UnityMIDIOut.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 27/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "UnityMIDIDestination.h"

@interface UnityMIDIDestination ()

@property (nonatomic, assign) UnityMIDIMessageHook destination;

@end

@implementation UnityMIDIDestination

-(id)initWithDestinationHook:(UnityMIDIMessageHook)destinationHook
{
    if (self = [super init])
    {
        self.destination = destinationHook;
    }
    return self;
}

-(void)handleOutputPackets:(const MIDIPacketList *)packetList
{
    if (self.destination != NULL)
    {
        self.destination(NULL);
    }
}

@end
