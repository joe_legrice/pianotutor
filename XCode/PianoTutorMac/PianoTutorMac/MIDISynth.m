//
//  UnityMIDISynth.m
//  PianoTutorMac
//
//  Created by MAG on 04/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDISynth.h"
#import "MIDIPacketListRingBuffer.h"
#import "Oscillator.h"
#import "DefaultOutput.h"
#import <AudioToolbox/AudioToolbox.h>

@interface MIDISynth()

@property (nonatomic, assign) AUGraph auGraph;
@property (atomic, strong) MIDIPacketListRingBuffer* buffer;

@end

@implementation MIDISynth

-(id)init
{
    if (self = [super init])
    {
        self.buffer = [[MIDIPacketListRingBuffer alloc] initWithSize:50];
        
        NewAUGraph(&_auGraph);
        AUGraphOpen(self.auGraph);
        
        Oscillator* osc = [[Oscillator alloc] init];
        [osc attachToGraph:self.auGraph];
        
        DefaultOutput* output = [[DefaultOutput alloc] init];
        [output attachToGraph:self.auGraph];
        
        [self connectSourceComponent:osc toDestinationComponent:output];

        AUGraphInitialize(self.auGraph);
        AUGraphStart (self.auGraph);
    }
    return self;
}

-(BOOL)connectSourceComponent:(ComponentBase*)sourceComponent
       toDestinationComponent:(ComponentBase*)destinationComponent
{
    OSStatus result = AUGraphConnectNodeInput(self.auGraph,
                                              sourceComponent.node, sourceComponent.bus,
                                              destinationComponent.node, destinationComponent.bus);
    return result;
}

OSStatus pullAudio(void* inRefCon, AudioUnitRenderActionFlags* ioActionFlags,
                   const AudioTimeStamp* inTimestamp, UInt32 inBusNumber, AudioBufferList* ioData)
{
    MIDISynth* self = (__bridge MIDISynth*)inRefCon;
    while ([self.buffer hasNext])
    {
        MIDIPacketList* packetList = [self.buffer popObject];
    }
    return 0;
}

-(void)handleOutputPackets:(const MIDIPacketList*)packetList
{
    [self.buffer pushObject:packetList];
}

@end
