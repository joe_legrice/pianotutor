//
//  MIDIPhysicalDevice.m
//  PianoTutorMac
//
//  Created by MAG on 01/03/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIPhysicalDevice.h"

@interface MIDIPhysicalDevice ()

@property (nonatomic, assign) MIDIDeviceRef device;

@end

@implementation MIDIPhysicalDevice

-(id)initWithDevice:(MIDIDeviceRef)device
{
    if (self = [super init])
    {
        self.device = device;
    }
    return self;
}

-(NSString*)getProperties
{
    CFPropertyListRef outProperties;
    MIDIObjectGetProperties(self.device, &outProperties, YES);
    CFTypeID typeId = CFGetTypeID(outProperties);
    
    id properties = nil;
    if (typeId == CFDictionaryGetTypeID())
    {
        properties = (__bridge NSDictionary*)outProperties;
        NSMutableDictionary* propDic = [NSMutableDictionary dictionaryWithDictionary:properties];
        for (id key in properties)
        {
            if ([[properties objectForKey:key] isKindOfClass:[NSData class]])
            {
                [propDic removeObjectForKey:key];
            }
        }
        properties = [NSDictionary dictionaryWithDictionary:propDic];
    }
    else if (typeId == CFArrayGetTypeID())
    {
        properties = (__bridge NSArray*)outProperties;
        NSMutableArray* propArr = [NSMutableArray arrayWithArray:properties];
        for (id val in properties)
        {
            if ([val isKindOfClass:[NSData class]])
            {
                [propArr removeObject:val];
            }
        }
        properties = [NSArray arrayWithArray:propArr];

    }
    else
    {
        return @"{}";
    }
    
    if ([NSJSONSerialization isValidJSONObject:properties])
    {
        NSError* error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:properties
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString* jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    else
    {
        return @"{\"error\" : \"could not serialize object\"}";
    }
}

-(uint)getNumberOfEntities
{
    return (uint)MIDIDeviceGetNumberOfEntities(self.device);
}

-(MIDIEntity*)getEntity:(uint)entityIndex
{
    MIDIEntityRef entity = MIDIDeviceGetEntity(self.device, entityIndex);
    return [[MIDIEntity alloc] initWithEntity:entity];
}

@end
