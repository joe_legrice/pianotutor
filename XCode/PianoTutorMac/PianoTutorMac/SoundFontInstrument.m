//
//  SoundFontInstrument.m
//  PianoTutorMac
//
//  Created by MAG on 22/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "SoundFontInstrument.h"

@implementation SoundFontInstrument

-(id)init
{
    if (self = [super initWithType:kAudioUnitType_MusicDevice andSubtype:kAudioUnitSubType_Sampler])
    {
        [self loadSoundFont];
    }
    return self;
}

-(OSStatus)loadSoundFont
{
    NSString* soundFontFilePath = @"/Users/MAG/Development/SourceTree/PianoTutor/XCode/PianoTutorMac/PianoTutorMac/FluidR3 GM2-2.SF2";
    NSURL* fileURL = [NSURL fileURLWithPath:soundFontFilePath];
    
    OSStatus result = noErr;
    
    AUSamplerInstrumentData instdata;
    instdata.fileURL  = (__bridge CFURLRef)fileURL;
    instdata.instrumentType = kInstrumentType_SF2Preset;
    instdata.bankMSB  = kAUSampler_DefaultMelodicBankMSB;
    instdata.bankLSB  = kAUSampler_DefaultBankLSB;
    instdata.presetID = 0;
    
    result = AudioUnitSetProperty(self.unit,
                                  kAUSamplerProperty_LoadInstrument,
                                  kAudioUnitScope_Global,
                                  0,
                                  &instdata,
                                  sizeof(instdata));
    
    return result;
}


@end
