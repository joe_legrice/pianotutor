//
//  TestSourceInput.h
//  PianoTutorMac
//
//  Created by MAG on 05/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIVirtualDevice.h"

@interface TestSourceInput : NSObject <MIDISource>

@property (nonatomic, assign) MIDIEndpointRef sourceEndpoint;

-(void)sendRandomNote;

@end