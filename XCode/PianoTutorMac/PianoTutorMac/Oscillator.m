//
//  Oscillator.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 21/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "Oscillator.h"

@interface Oscillator()

@property (nonatomic, assign) float frequency;
@property (nonatomic, assign) Float64 sampleRate;
@property (nonatomic, assign) long sampleNum;

@end

@implementation Oscillator

float generateAudio(Oscillator* self)
{
    Float64 freq = self.frequency;
    if (freq > 0.01f)
    {
        long period_samples = self.sampleRate / freq;
        if (period_samples > 0)
        {
            float x = (self.sampleNum / (float)period_samples);
            float value = sinf(2.0f * M_PI * x);
            self.sampleNum = (self.sampleNum + 1) % period_samples;
    
            return value;
        }
    }
    return 0.0f;
}

OSStatus RenderTone(void *inRefCon,
                    AudioUnitRenderActionFlags *ioActionFlags,
                    const AudioTimeStamp *inTimeStamp,
                    UInt32 inBusNumber,
                    UInt32 inNumberFrames,
                    AudioBufferList *ioData)

{    Oscillator* generator = (__bridge Oscillator*)inRefCon;
    
    for (int buffer = 0; buffer < ioData->mNumberBuffers; buffer++) {
        memset(ioData->mBuffers[buffer].mData, 0, inNumberFrames * sizeof(AudioUnitSampleType));
    }
    
    if (generator.active)
    {
//        mach_timebase_info_data_t tinfo;
//        mach_timebase_info(&tinfo);
//        double hTime2nsFactor = (double)tinfo.numer / tinfo.denom;
//        double nanoseconds = inTimeStamp->mHostTime * hTime2nsFactor;
//        
//        double seconds = (nanoseconds) / 1000000000.0;
//        seconds -= floor(seconds);
        
        // mono, only fill one channel
        AudioUnitSampleType* audio = (AudioUnitSampleType*)ioData->mBuffers[0].mData;
        for (int frame = 0; frame < inNumberFrames; frame++) {
            float sample = generateAudio(generator);
            audio[frame] = sample * 16777216L;
        }
    }
    return noErr;
}

-(id)init
{
    if (self = [super initWithType:kAudioUnitType_Effect andSubtype:kAudioUnitSubType_GraphicEQ])
    {
        self.frequency = 50.0f;
        self.active = YES;
    }
    return self;
}

-(void)attachToGraph:(AUGraph)graph
{
    [super attachToGraph:graph];
    self.sampleRate = [self outputSampleRate];
    [self attachRenderCallback:RenderTone toBus:0 inGraph:graph];
}

@end
