//
//  UnityMIDIIn.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 27/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "UnityMIDISource.h"

@implementation UnityMIDISource

-(void)didReceiveData:(Byte*)data
{
    MIDIPacketList* pktlist;
    MIDIPacketListInit(pktlist);
    MIDIReceived(self.sourceEndpoint, pktlist);
}

@end
