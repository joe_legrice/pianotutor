//
//  UnityMIDI.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 27/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "UnityMIDI.h"
#import "UnityMIDISource.h"
#import "UnityMIDIDestination.h"

#import "MIDI.h"
#import "MIDIPhysicalDevice.h"
#import "MIDIPhysicalDeviceManager.h"


static UnityMIDI* s_staticInstance;
static MIDIPhysicalDeviceManager* s_staticDeviceManager;

@interface UnityMIDI ()

@property (nonatomic, strong) MIDI* midi;
@property (nonatomic, strong) MIDIVirtualDevice* virtualDevice;
@property (nonatomic, strong) UnityMIDISource* unitySource;
@property (nonatomic, strong) UnityMIDIDestination* unityDestination;

@end

@implementation UnityMIDI

-(id)initWithDestinationHook:(UnityMIDIMessageHook)destinationHook
{
    if (self = [super init])
    {
        self.midi = [[MIDI alloc] init];
        self.unitySource = [[UnityMIDISource alloc] init];
        self.unityDestination = [[UnityMIDIDestination alloc] initWithDestinationHook:destinationHook];
        self.virtualDevice = [[MIDIVirtualDevice alloc] initWithSource:self.unitySource
                                                            andDestination:self.unityDestination];
    }
    return self;
}

@end

void InitialiseUnityMIDI(UnityMIDIMessageHook destinationHook)
{
    if (s_staticInstance == nil)
    {
        s_staticInstance = [[UnityMIDI alloc] init];
    }
}

void SendMIDI(Byte* data)
{
    if (s_staticInstance != nil && s_staticInstance.unitySource != nil)
    {
        [s_staticInstance.unitySource didReceiveData:data];
    }
}

const char* GetDevicePropertiesJSON()
{
    if (s_staticDeviceManager == NULL)
    {
        s_staticDeviceManager = [[MIDIPhysicalDeviceManager alloc] init];
    }
    
    NSMutableString* jsonString = [NSMutableString stringWithString:@"[ "];
    for (int deviceIndex = 0; deviceIndex < s_staticDeviceManager.allDevices.count; deviceIndex++)
    {
        MIDIPhysicalDevice* device = s_staticDeviceManager.allDevices[deviceIndex];
        [jsonString appendFormat:@"%@", [device getProperties]];
        
        if (deviceIndex < s_staticDeviceManager.allDevices.count - 1)
        {
            [jsonString appendFormat:@", "];
        }
    }
    [jsonString appendFormat:@" ]"];
    
    return [jsonString UTF8String];
}
