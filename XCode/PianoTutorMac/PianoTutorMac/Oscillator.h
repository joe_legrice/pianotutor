//
//  Oscillator.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 21/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "ComponentBase.h"

@interface Oscillator : ComponentBase

@property (nonatomic, assign) BOOL active;

@end