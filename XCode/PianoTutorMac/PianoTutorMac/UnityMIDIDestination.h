//
//  UnityMIDIOut.h
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 27/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIVirtualDevice.h"
#import "UnityMIDI.h"

@interface UnityMIDIDestination : NSObject <MIDIDestination>

-(id)initWithDestinationHook:(UnityMIDIMessageHook)destinationHook;

@end
