//
//  UnityMIDI.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 31/01/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDI.h"

@interface MIDI ()

@property (nonatomic, assign) MIDIClientRef midiClient;

@property (nonatomic, assign) MIDIPortRef inputPort;
@property (nonatomic, assign) MIDIPortRef outputPort;

@property (nonatomic, assign) MIDIEndpointRef* destinationEndpoint;

@end

@implementation MIDI : NSObject

-(id)init
{
    if (self = [super init])
    {
        MIDIClientCreate(CFSTR("UnityMIDI Client"), MIDINotify, NULL, &_midiClient);
        MIDIInputPortCreate(self.midiClient, (CFStringRef)@"UnityMIDIIn",
                            MIDIRead, (__bridge void*)self, &_inputPort);
        MIDIOutputPortCreate(self.midiClient, CFSTR("UnityMIDIOut"), &_outputPort);
    }
    return self;
}

-(void)connectDeviceSource:(id<MIDIDevice>)device
{
    [device attachSourceToMIDIConnection:&_midiClient inputPort:self.inputPort];
}

-(void)connectDeviceDestination:(id<MIDIDevice>)device
{
    if (self.destinationEndpoint == NULL)
    {
        self.destinationEndpoint = [device attachDestinationToMIDIConnection:&_midiClient];
    }
}

void MIDINotify(const MIDINotification* message, void* refCon)
{
    NSLog(@"MIDI Notify, messageId=%d,", message->messageID);
}

void MIDIRead(const MIDIPacketList* pktlist, void* readProcRefCon, void* srcConnRefCon)
{
    NSLog(@"MIDI Read");
    MIDI* self = (__bridge MIDI*)readProcRefCon;
    if (self.destinationEndpoint != NULL)
    {
        MIDISend(self.outputPort, *self.destinationEndpoint, pktlist);
    }
}

@end