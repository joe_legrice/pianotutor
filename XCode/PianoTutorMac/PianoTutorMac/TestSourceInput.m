//
//  TestSourceInput.m
//  PianoTutorMac
//
//  Created by MAG on 05/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "TestSourceInput.h"

@implementation TestSourceInput : NSObject

UInt8 RandomNoteNumber() { return (UInt8)(rand() / (RAND_MAX / 127)); }

-(void)sendRandomNote
{
    const UInt8 note      = RandomNoteNumber();
    const UInt8 noteOn[]  = { 0x90, note, 127 };
    const UInt8 noteOff[] = { 0x80, note, 0   };
    
    ByteCount pktListBufferSize = sizeof(noteOn)+sizeof(noteOff)+100;
    Byte pktListBuffer[pktListBufferSize];
    
    MIDIPacketList* pktList = (MIDIPacketList*)pktListBuffer;
    MIDIPacket* pkt = MIDIPacketListInit(pktList);
    
    pkt = MIDIPacketListAdd(pktList, sizeof(pktListBuffer), pkt, 0, sizeof(noteOn), noteOn);
    MIDIPacketListAdd(pktList, sizeof(pktListBuffer), pkt, 0.1, sizeof(noteOff), noteOff);
    
    MIDIReceived(self.sourceEndpoint, pktList);
}

@end