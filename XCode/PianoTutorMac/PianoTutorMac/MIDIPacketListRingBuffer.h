//
//  RingBuffer.h
//  PianoTutorMac
//
//  Created by MAG on 10/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMIDI/MIDIServices.h>

@interface MIDIPacketListRingBuffer : NSObject {
    
}

-(id)initWithSize:(int)size;
-(void)pushObject:(const MIDIPacketList*)object;
-(MIDIPacketList*)popObject;
-(BOOL)hasNext;

@end