//
//  TestDestinationOutput.m
//  PianoTutorMac
//
//  Created by Joseph Le Grice on 07/02/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "TestDestinationOutput.h"

@implementation TestDestinationOutput : NSObject

-(void)handleOutputPackets:(const MIDIPacketList*)packetList
{
    for (int i = 0; i < packetList->numPackets; i++)
    {
        const MIDIPacket* packet = &packetList->packet[0];
        
        int j=0;
        while (true)
        {
            if (packet->data[j] == 0)
            {
                break;
            }
            NSLog(@"%02x: %02x %02x", packet->data[j], packet->data[j+1], packet->data[j+2]);
            j += 3;
        }
    }
}

@end