//
//  MIDIEntity.m
//  PianoTutorMac
//
//  Created by MAG on 01/03/2016.
//  Copyright © 2016 Joseph Le Grice. All rights reserved.
//

#import "MIDIEntity.h"

@interface MIDIEntity ()

@property (nonatomic, assign) MIDIEntityRef entity;

@end

@implementation MIDIEntity

-(id)initWithEntity:(MIDIEntityRef)entity
{
    if (self = [super init])
    {
        self.entity = entity;
    }
    return self;
}

-(uint)numberOfSources
{
    return (uint)MIDIEntityGetNumberOfSources(self.entity);
}

-(uint)numberOfDestinations
{
    return (uint)MIDIEntityGetNumberOfDestinations(self.entity);
}

-(MIDIEndpointRef)getSource:(uint)sourceIndex
{
    return MIDIEntityGetSource(self.entity, sourceIndex);
}

-(MIDIEndpointRef)getDestination:(uint)destinationIndex
{
    return MIDIEntityGetDestination(self.entity, destinationIndex);
}

@end
