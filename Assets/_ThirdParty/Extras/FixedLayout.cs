﻿using UnityEngine;
using System.Collections;

public class FixedLayout : LayoutController
{
	public enum HorizontalAlignment
	{
		Left,
		Centre,
		Right
	}

	public enum VerticalAlignment
	{
		Top,
		Middle,
		Bottom
	}

	[SerializeField] private VerticalAlignment m_verticalAlignment = VerticalAlignment.Top;
	[SerializeField] private HorizontalAlignment m_horizontalAlignment = HorizontalAlignment.Left;

	[SerializeField] private VerticalAlignment m_cellVerticalAlignment = VerticalAlignment.Middle;
	[SerializeField] private HorizontalAlignment m_cellHorizontalAlignment = HorizontalAlignment.Centre;

    [SerializeField] private bool m_ignoreBounds;

	[SerializeField] private Vector2 m_cellDimensions;
	[SerializeField] private Vector2 m_cellPadding;
	[SerializeField] private int m_numberOfColumns;

    public override Vector2 GetBounds()
    {
        return m_cellDimensions + m_cellPadding;
    }

    public override void RepositionChildren()
	{
		int numberOfActiveChildren = GetNumberOfActiveChildren();

		int numberOfColumns = numberOfActiveChildren < m_numberOfColumns ? numberOfActiveChildren : m_numberOfColumns;
		int numberOfRows = Mathf.CeilToInt((float)numberOfActiveChildren / numberOfColumns);

		Vector2 totalLayoutSize = Vector2.zero;
		totalLayoutSize.x = numberOfColumns * m_cellDimensions.x + (numberOfColumns-1)*m_cellPadding.x;
		totalLayoutSize.y = numberOfRows * m_cellDimensions.y + (numberOfRows-1)*m_cellPadding.y;

		int layoutIndex = -1;
		for(int childIndex = 0; childIndex < transform.childCount; childIndex++)
		{
			Transform childTransform = transform.GetChild(childIndex);
			if (childTransform.gameObject.activeSelf)
			{
				layoutIndex++;
				int columnIndex = layoutIndex % numberOfColumns;
				int rowIndex = layoutIndex / numberOfColumns;

                Vector3 position = Vector3.zero;
				position.x = m_cellDimensions.x * columnIndex + m_cellPadding.x * columnIndex;
				position.y -= m_cellDimensions.y * rowIndex + m_cellPadding.y * rowIndex;

                if (!m_ignoreBounds)
                {
                    switch (m_cellHorizontalAlignment)
                    {
                        case HorizontalAlignment.Left: { position.x -= 0.5f * m_cellDimensions.x; break; }
                        case HorizontalAlignment.Centre: { break; }
                        case HorizontalAlignment.Right: { position.x += 0.5f * m_cellDimensions.x; break; }
                    }

                    switch (m_cellVerticalAlignment)
                    {
                        case VerticalAlignment.Top: { position.y -= 0.5f * m_cellDimensions.y; break; }
                        case VerticalAlignment.Middle: { break; }
                        case VerticalAlignment.Bottom: { position.y += 0.5f * m_cellDimensions.y; break; }
                    }
                }

                switch (m_horizontalAlignment)
                {
                    case HorizontalAlignment.Left: { break; }
                    case HorizontalAlignment.Centre: { position.x -= 0.5f * totalLayoutSize.x; break; }
                    case HorizontalAlignment.Right: { position.x -= totalLayoutSize.x; break; }
                }

                switch (m_verticalAlignment)
                {
                    case VerticalAlignment.Top: { break; }
                    case VerticalAlignment.Middle: { position.y += 0.5f * totalLayoutSize.y; break; }
                    case VerticalAlignment.Bottom: { position.y += totalLayoutSize.y; break; }
                }

                childTransform.localPosition = position;
			}
		}
	}

    public void SetNumberOfColumns(int columns)
    {
        m_numberOfColumns = columns;
        RepositionChildren();
    }

    public int GetNumberOfActiveChildren()
	{
		int result = 0;
		foreach (Transform t in transform)
		{
			if (t.gameObject.activeSelf)
			{
				result++;
			}
		}
		return result;
	}

	public void CalculateMaximumCellDimensions()
	{
		Vector2 biggestBounds = Vector2.zero;
		foreach(Transform childTransform in transform)
		{
			if(childTransform.gameObject.activeSelf)
			{
				Bounds childBounds = DisplayUtils.GetRendererBoundsInChildren(childTransform);
				Vector2 thisBoundSize = Vector2.zero;
				thisBoundSize.x = childBounds.size.x * childTransform.localScale.x;
				thisBoundSize.y = childBounds.size.y * childTransform.localScale.y;

				if (thisBoundSize.x >= biggestBounds.x && thisBoundSize.y >= biggestBounds.y)
				{
					biggestBounds.x = thisBoundSize.x;
					biggestBounds.y = thisBoundSize.y;
				}
			}
		}
		m_cellDimensions = biggestBounds;
	}
}
