using UnityEngine;
using System.Collections;

public static class DisplayUtils
{
	// Returns bounds in WORLD SPACE
    public static Bounds GetRendererBoundsOfGameObjectAndAllChildren(GameObject gameObject)
    {
        Bounds bounds;

        if (gameObject.GetComponent<Renderer>() != null)
        {
            bounds = gameObject.GetComponent<Renderer>().bounds;
        }
        else
        {
            bounds = new Bounds(gameObject.transform.position, Vector3.zero);
        }

        foreach(Renderer childRenderer in gameObject.GetComponentsInChildren<Renderer>())
        {
            bounds.Encapsulate(childRenderer.bounds);
        }

        return bounds;
    }

	// ???????????????????????????????????????????
	// Matrix Math Magic from tk2dUIScrollableArea
	// ???????????????????????????????????????????
	public static Bounds GetRendererBoundsInChildren(Transform t) {
		Vector3 vector3Min = new Vector3(float.MinValue, float.MinValue, float.MinValue);
		Vector3 vector3Max = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
		Vector3[] minMax = new Vector3[] {
			vector3Max,
			vector3Min
		};
		GetRendererBoundsInChildren(t.worldToLocalMatrix, minMax, t);

		if (minMax[0] != vector3Max && minMax[1] != vector3Min) {
			minMax[0] = Vector3.Min(minMax[0], Vector3.zero);
			minMax[1] = Vector3.Max(minMax[1], Vector3.zero);

			Bounds b = new Bounds();
			b.min = minMax[0];
			b.max = minMax[1];

			return b;
		}
		else {
			return new Bounds();
		}
	}

	private static readonly Vector3[] boxExtents = new Vector3[] {
		new Vector3(-1, -1, -1), new Vector3( 1, -1, -1), new Vector3(-1,  1, -1), new Vector3( 1,  1, -1), new Vector3(-1, -1,  1), new Vector3( 1, -1,  1), new Vector3(-1,  1,  1), new Vector3( 1,  1,  1)
	};

	private static void GetRendererBoundsInChildren(Matrix4x4 rootWorldToLocal, Vector3[] minMax, Transform t) {
		MeshFilter mf = t.GetComponent<MeshFilter>();
		if (mf != null && mf.sharedMesh != null) {
			Bounds b = mf.sharedMesh.bounds;
			Matrix4x4 relativeMatrix = rootWorldToLocal * t.localToWorldMatrix;
			for (int j = 0; j < 8; ++j) {
				Vector3 localPoint = b.center + Vector3.Scale(b.extents, boxExtents[j]);
				Vector3 pointRelativeToRoot = relativeMatrix.MultiplyPoint(localPoint);
				minMax[0] = Vector3.Min(minMax[0], pointRelativeToRoot);
				minMax[1] = Vector3.Max(minMax[1], pointRelativeToRoot);
			}
		}
		int childCount = t.childCount;
		for (int i = 0; i < childCount; ++i) {
			Transform child = t.GetChild(i);
			#if UNITY_3_5
			if (t.gameObject.active) {
			#else
			if (t.gameObject.activeSelf) {
			#endif
				GetRendererBoundsInChildren(rootWorldToLocal, minMax, child);
			}
		}
	}
	// ???????????????????????????????????????????
	// Matrix Math Magic from tk2dUIScrollableArea
	// ???????????????????????????????????????????

	// Returns bounds in LOCAL SPACE
	// TODO: Fix this - Currently gicing inconsistent results
	public static Bounds GetMeshBoundsOfGameObjectAndAllChildren(GameObject gameObject)
	{
		Bounds bounds;

		if (gameObject.GetComponent<MeshFilter>() != null)
		{
			bounds = gameObject.GetComponent<MeshFilter>().mesh.bounds;
		}
		else
		{
			bounds = new Bounds(gameObject.transform.position, Vector3.zero);
		}
		
		foreach(MeshFilter childMeshFilter in gameObject.GetComponentsInChildren<MeshFilter>(true))
		{
			bounds.Encapsulate(childMeshFilter.mesh.bounds);
		}
		
		return bounds;
	}

    public static void DisableRenderer(GameObject target)
    {
        DisableRenderer(target, true);
    }

    public static void DisableRenderer(GameObject target, bool includeAllChildren)
    {
        SetRendererState(target, includeAllChildren, false);
    }

    public static void EnableRenderer(GameObject target)
    {
        EnableRenderer(target, true);
    }

    public static void EnableRenderer(GameObject target, bool includeAllChildren)
    {
        SetRendererState(target, includeAllChildren, true);
    }

    public static void DisableCollider(GameObject target, bool includeAllChildren = true)
    {
        SetColliderState(target, includeAllChildren, false);
    }

    public static void EnsableCollider(GameObject target, bool includeAllChildren = true)
    {
        SetColliderState(target, includeAllChildren, true);
    }

    public static void DisableRendererAndCollider(GameObject target, bool includeAllChildren = true)
    {
        SetRendererState(target, includeAllChildren, false);
        SetColliderState(target, includeAllChildren, false);
    }

    public static void EnableRendererAndCollider(GameObject target, bool includeAllChildren = true)
    {
        SetRendererState(target, includeAllChildren, true);
        SetColliderState(target, includeAllChildren, true);
    }

    private static void SetRendererState(GameObject target, bool includeAllChildren, bool state)
    {
        if (target.GetComponent<Renderer>() != null)
        {
            target.GetComponent<Renderer>().enabled = state;
        }

        if (includeAllChildren)
        {
            foreach(Renderer childRenderer in target.GetComponentsInChildren<Renderer>())
            {
                childRenderer.enabled = state;
            }
        }
    }

    private static void SetColliderState(GameObject target, bool includeAllChildren, bool state)
    {
        if (target.GetComponent<Collider>() != null)
        {
            target.GetComponent<Collider>().enabled = state;
        }
        
        if (includeAllChildren)
        {
            foreach(Collider childCollider in target.GetComponentsInChildren<Collider>())
            {
                childCollider.enabled = state;
            }
        }
    }

    public static void SetSortingLayerName(GameObject target, string sortingLayerName, bool includeAllChildren)
    {
        if (target.GetComponent<Renderer>() != null)
        {
            target.GetComponent<Renderer>().sortingLayerName = sortingLayerName;
        }
        
        if (includeAllChildren)
        {
            foreach(Renderer childRenderer in target.GetComponentsInChildren<Renderer>())
            {
                childRenderer.sortingLayerName = sortingLayerName;
            }
        }
    }

    public static void SetLayer(GameObject target, int layer, bool includeAllChildren)
    {
        target.layer = layer;
        
        if (includeAllChildren)
        {
            foreach(Transform childTransform in target.GetComponentsInChildren<Transform>())
            {
                childTransform.gameObject.layer = layer;
            }
        }
    }
}

