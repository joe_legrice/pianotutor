﻿using UnityEngine;
using System.Collections;

public abstract class LayoutController : MonoBehaviour
{
	public abstract void RepositionChildren();

    public abstract Vector2 GetBounds();
}
