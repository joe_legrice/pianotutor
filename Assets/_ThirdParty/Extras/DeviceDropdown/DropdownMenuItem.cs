﻿using UnityEngine;

using System;

public class DropdownMenuItem : MonoBehaviour
{
    [SerializeField]
    private tk2dUIItem m_uiItem;
    [SerializeField]
    private tk2dTextMesh m_displayTextMesh;

    private int m_index;
    private Action<int> m_callback;
    private Transform m_transform;

    public void Setup(string displayText, Action<int> callback, int index)
    {
        m_displayTextMesh.text = displayText;
        m_index = index;
        m_callback = callback;
        m_uiItem.OnClick += OnSelected;
    }

    private void Start()
    {
        m_transform = gameObject.GetComponent<Transform>();
    }

    private void OnSelected()
    {
        m_callback(m_index);
    }
}
