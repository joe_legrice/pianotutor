﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DropdownMenu))]
public class DropdownMenuEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Resize"))
        {
            ((DropdownMenu)target).Resize();
        }
    }
}
