﻿using UnityEngine;

using System;
using System.Collections.Generic;

public class DropdownMenu : MonoBehaviour
{
    [SerializeField] private GameObject m_dropdownItemPrefab;
    [SerializeField] private tk2dUIItem m_uiItem;
    [SerializeField] private Vector2 m_size;
    [SerializeField] private GameObject m_openHierarchy;
    [SerializeField] private GameObject m_closedHierarchy;
    [SerializeField] private tk2dSlicedSprite m_activeBackgroundSprite;
    [SerializeField] private tk2dTextMesh m_selectedTextMesh;
    [SerializeField] private Transform m_dropdownItemParentTransform;

    private bool m_open;
    private int m_currentIndex;
    private Func<int, bool> m_callback;
    private List<string> m_displayText;
    
    public void Setup(List<string> displayText, int indexSelected, Func<int, bool> newIndexSelectedCallback)
    {
        m_displayText = displayText;
        m_currentIndex = indexSelected;
        m_callback = newIndexSelectedCallback;

        m_selectedTextMesh.text = displayText[indexSelected];

        foreach (Transform t in m_dropdownItemParentTransform)
        {
            Destroy(t);
        }

        Resize();
    }

    public void Resize()
    {
        float height = 0.0f;
        if (m_displayText != null && m_displayText.Count > 0)
        {
            height = m_displayText.Count * m_size.y;
        }
        m_activeBackgroundSprite.dimensions = new Vector2(m_size.x, height);
    }

    private void Start()
    {
        m_uiItem.OnClick += ToggleMenu;
        m_openHierarchy.SetActive(false);
        m_closedHierarchy.SetActive(true);
    }

    private void ToggleMenu()
    {
        m_open = !m_open;
        if (m_open)
        {
            ShowOptions();
        }
        else
        {
            HideOptions();
        }
    }

    private void ShowOptions()
    {
        m_openHierarchy.SetActive(true);
        m_closedHierarchy.SetActive(false);

        if (m_dropdownItemParentTransform.childCount == 0)
        {
            for (int i = 0; i < m_displayText.Count; i++)
            {
                if (i != m_currentIndex)
                {
                    GameObject itemInstance = Instantiate(m_dropdownItemPrefab);
                    Transform itemInstanceTransform = itemInstance.GetComponent<Transform>();
                    itemInstanceTransform.parent = m_dropdownItemParentTransform;

                    DropdownMenuItem ddi = itemInstance.GetComponent<DropdownMenuItem>();
                    ddi.Setup(m_displayText[i], HandleCallback, i);
                }
            }
        }
        m_dropdownItemParentTransform.GetComponent<LayoutController>().RepositionChildren();
    }

    private void HideOptions()
    {
        m_openHierarchy.SetActive(false);
        m_closedHierarchy.SetActive(true);
    }

    private void HandleCallback(int indexSelected)
    {
        HideOptions();
        if (m_callback(indexSelected))
        {
            m_currentIndex = indexSelected;
            m_selectedTextMesh.text = m_displayText[indexSelected];
        }
    }
}
