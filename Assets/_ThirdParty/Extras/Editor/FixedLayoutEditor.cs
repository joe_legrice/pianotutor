using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FixedLayout))]
public class FixedLayoutEditor : Editor
{
	public FixedLayout Target { get{ return ((FixedLayout)target); } }

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		EditorGUILayout.LabelField("Anchor Point Alignment", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(serializedObject.FindProperty("m_verticalAlignment"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("m_horizontalAlignment"));

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Cell Alignment", EditorStyles.boldLabel);
        SerializedProperty ignoreBounds = serializedObject.FindProperty("m_ignoreBounds");
        EditorGUILayout.PropertyField(ignoreBounds);

        if (!ignoreBounds.boolValue)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_cellVerticalAlignment"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_cellHorizontalAlignment"));
        }
        else
        {
            serializedObject.FindProperty("m_cellVerticalAlignment").enumValueIndex = 1;
            serializedObject.FindProperty("m_cellHorizontalAlignment").enumValueIndex = 1;
        }

        EditorGUILayout.Space();
		EditorGUILayout.LabelField("Other Stuff", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(serializedObject.FindProperty("m_cellDimensions"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("m_cellPadding"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("m_numberOfColumns"));

		ValidateProperties();

		serializedObject.ApplyModifiedProperties();

		if (GUILayout.Button("Figure out Cell Dimensions"))
		{
			Target.CalculateMaximumCellDimensions();
		}
		if (GUILayout.Button("Reposition"))
		{
			Target.RepositionChildren();
		}
	}

	private void ValidateProperties()
	{
		Vector2 vec = serializedObject.FindProperty("m_cellDimensions").vector2Value;
		if (vec.x <= 0)
		{
			vec.x = 1;
		}
		if (vec.y <= 0)
		{
			vec.y = 1;
		}
		serializedObject.FindProperty("m_cellDimensions").vector2Value = vec;

		if (serializedObject.FindProperty("m_numberOfColumns").intValue <= 0)
		{
			serializedObject.FindProperty("m_numberOfColumns").intValue = 1;
		}
	}
}
