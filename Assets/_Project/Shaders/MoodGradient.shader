﻿Shader "PianoTutor/MoodGradient"
{
	Properties
	{
		_Factor("Factor", Float) = 1.0
		_PrimaryColor("Primary Color", Color) = (0.0, 0.0, 1.0, 1.0)
		_SecondaryColor("Secondary Color", Color) = (0.0, 0.0, 0.0, 1.0)
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float _Factor;
			uniform float4 _PrimaryColor;
			uniform float4 _SecondaryColor;

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 col : TEXCOORD0;
			};

			v2f vert (float4 vertexPos : POSITION)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, vertexPos);
				o.col = _Factor * (1.0 - (vertexPos[1] + 0.5)) * _SecondaryColor + (vertexPos[1] + 0.5) * _PrimaryColor;

				return o;
			}
			
			float4 frag (v2f i) : COLOR
			{
				return i.col;
			}
			ENDCG
		}
	}
}
