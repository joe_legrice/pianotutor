﻿using UnityEngine;
using UnityMidiKit;

using System.Collections.Generic;

public class NoteSpawner : MonoBehaviour, ISourceListener
{
    [SerializeField] private PianoController m_piano;
    [SerializeField] private GameObject m_ascendingNotePrefab;
    [SerializeField] private Transform m_parentTransform;

    private bool m_running;
    private Dictionary<Pitch, AscendingNote> m_currentAscendingNotes = new Dictionary<Pitch, AscendingNote>();

    public void SetRunning(bool running)
    {
        m_running = running;
        if (!m_running)
        {
            List<Pitch> pitches = new List<Pitch>(m_currentAscendingNotes.Keys);
            foreach (Pitch p in pitches)
            {
                m_currentAscendingNotes[p].ReleaseNote();
                m_currentAscendingNotes.Remove(p);
            }
        }
    }

    public void OnSourceConnected(MIDISourceEntity se)
    {
        se.OnNotePressed += NotePressed;
        se.OnNoteReleased += NoteReleased;
    }

    public void OnSourceDisconnected(MIDISourceEntity se)
    {
        se.OnNotePressed -= NotePressed;
        se.OnNoteReleased -= NoteReleased;
    }

    private void NotePressed(MIDINoteOnMessage msg)
    {
        if (m_running && !m_currentAscendingNotes.ContainsKey(msg.Pitch))
        {
            PianoKey pianoKey = m_piano.GetKey(msg.Pitch);
            GameObject anObj = Instantiate(m_ascendingNotePrefab);
            anObj.GetComponent<Transform>().parent = m_parentTransform;
            AscendingNote an = anObj.GetComponent<AscendingNote>();
            an.SetNoteOn(pianoKey);
            m_currentAscendingNotes[msg.Pitch] = an;
        }
    }

    private void NoteReleased(MIDINoteOffMessage msg)
    {
        if (m_running && m_currentAscendingNotes.ContainsKey(msg.Pitch))
        {
            m_currentAscendingNotes[msg.Pitch].ReleaseNote();
            m_currentAscendingNotes.Remove(msg.Pitch);
        }
    }

    private void Awake()
    {
        DeviceManager.Instance.RegisterDeviceListener(this);
    }
}
