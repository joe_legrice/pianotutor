﻿using UnityEngine;
using UnityMidiKit;

using System.Collections.Generic;

public class SongSpawner : MonoBehaviour
{
    [SerializeField] private bool m_debug;
    [SerializeField] private PianoController m_piano;
    [SerializeField] private HierarchyController m_hierarchyControl;
    [SerializeField] private GameObject m_barPrefab;
    [SerializeField] private GameObject m_notePrefab;
    [SerializeField] private Transform m_parentTransform;
    [SerializeField] private float m_unitsPerTick = 1;

    private List<GameObject> m_barLines = new List<GameObject>();
    private List<DescendingNote> m_descendingNotes = new List<DescendingNote>();

    public float UnitsPerTick { get { return m_unitsPerTick; } }
    public MIDISequenceStepper LoadedSequenceStepper { get; private set; }

    public void LoadSongAtPath(string path)
    {
        foreach (DescendingNote dn in m_descendingNotes)
        {
            DestroyImmediate(dn.gameObject);
        }
        m_descendingNotes.Clear();

        foreach (GameObject go in m_barLines)
        {
            DestroyImmediate(go);
        }
        m_barLines.Clear();

        MIDISequence sequence = MIDIParser.ParseFile(path);
        LoadedSequenceStepper = new MIDISequenceStepper(sequence);

        foreach (MIDINote mn in LoadedSequenceStepper.AllNotes)
        {
            GameObject newObject = Instantiate(m_notePrefab);
            Transform newObjectTransform = newObject.GetComponent<Transform>();
            newObjectTransform.parent = m_parentTransform;

            DescendingNote dn = newObject.GetComponent<DescendingNote>();
            dn.Setup(mn, m_piano.GetKey(mn.Pitch), m_unitsPerTick);

            m_descendingNotes.Add(dn);
        }

        for (int bar = 0; bar < LoadedSequenceStepper.NumberOfBars; bar++)
        {
            GameObject barObject = Instantiate(m_barPrefab);
            m_barLines.Add(barObject);

            Transform barObjectTransform = barObject.GetComponent<Transform>();
            barObjectTransform.parent = m_parentTransform;
            BarLine bl = barObject.GetComponent<BarLine>();
            bl.Initialise(bar, sequence.TicksPerQuarterNote, m_unitsPerTick);
        }

        m_hierarchyControl.SetState(true);
    }
}
