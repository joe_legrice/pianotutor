﻿using UnityEngine;
using UnityMidiKit;
using System.Collections;

public class Startup : MonoBehaviour
{
    private void Start()
    {
        MIDI.Initialise();
        SaveDataManager.Instance.CacheLoadDataFromDisk();
        PracticeSessionStats.Instance.Load();
    }
}
