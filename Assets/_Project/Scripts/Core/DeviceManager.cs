﻿using UnityEngine;
using UnityMidiKit;

using System.Collections.Generic;

public class DeviceManager : Singleton<DeviceManager>
{
    private MIDISourceEntity m_currentSource;
    private List<ISourceListener> m_deviceListeners = new List<ISourceListener>();

    public MIDISourceEntity CurrentSource { get { return m_currentSource; } }

    public bool SelectSource(MIDISourceEntity se)
    {
        if (m_currentSource == se)
        {
            return !DisconnectSource();
        }
        else
        {
            if (DisconnectSource())
            {
                if (se != null && se.StartReceiving())
                {
                    m_currentSource = se;
                    foreach (ISourceListener sl in m_deviceListeners)
                    {
                        sl.OnSourceConnected(m_currentSource);
                    }
                }
            }
            return m_currentSource == se;
        }
    }

    public bool DisconnectSource()
    {
        if (m_currentSource != null && m_currentSource.Close())
        {
            foreach (ISourceListener sl in m_deviceListeners)
            {
                sl.OnSourceDisconnected(m_currentSource);
            }
            m_currentSource = null;
        }
        return m_currentSource == null;
    }

    public void RegisterDeviceListener(ISourceListener dl)
    {
        if (m_currentSource != null)
        {
            dl.OnSourceConnected(m_currentSource);
        }
        m_deviceListeners.Add(dl);
    }

    public void RemoveDeviceListener(ISourceListener dl)
    {
        if (m_currentSource != null)
        {
            dl.OnSourceDisconnected(m_currentSource);
        }
        m_deviceListeners.Remove(dl);
    }


    private void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            DisconnectSource();
        }
    }

    private void OnApplicationQuit()
    {
        DisconnectSource();
    }
}

public interface ISourceListener
{
    void OnSourceConnected(MIDISourceEntity se);
    void OnSourceDisconnected(MIDISourceEntity se);
}