﻿using UnityEngine;
using System.Collections.Generic;

public class SongLibrary : MonoBehaviour
{
    [SerializeField] private SongSpawner m_songSpawner;
    [SerializeField] private ScrollViewSpawner m_spawner;
    [SerializeField] private Song[] m_songs;

    [SerializeField] private tk2dUIItem m_showSongSelectButton;
    [SerializeField] private tk2dUIItem m_hideSongSelectButton;
    [SerializeField] private HierarchyController m_hierarchyControl;

    private List<SongOption> m_options;

    private void Start()
    {
        m_showSongSelectButton.OnClick += () => {
            m_hierarchyControl.SetState(true);
        };
        m_hideSongSelectButton.OnClick += () => {
            m_hierarchyControl.SetState(false);
        };

        m_spawner.RefreshItems(m_songs.Length);
        m_options = m_spawner.GetComponentInObjects<SongOption>();
        for (int songIndex = 0; songIndex < m_songs.Length; songIndex++)
        {
            m_options[songIndex].Setup(m_songs[songIndex], HandleSongSelect);
        }
    }

    private void HandleSongSelect(Song s)
    {
        m_hierarchyControl.SetState(false);
        m_songSpawner.LoadSongAtPath(s.SongPath);
    }
}
