﻿using UnityEngine;
using UnityMidiKit;

using System.Collections;
using System.Collections.Generic;

public class SongScroller : MonoBehaviour, ISourceListener
{
    [SerializeField] private SongSpawner m_songSpawner;
    [SerializeField] private SongScrollView m_scrollView;
    [SerializeField] private float m_scrollSpeed = 0.01f; // TODO: Use Tempo somehow maybe?

    private List<Pitch> m_keyDowns = new List<Pitch>();
    private float m_currentPosition;

    private void Start()
    {
        m_keyDowns = new List<Pitch>();
        DeviceManager.Instance.RegisterDeviceListener(this);

        StartCoroutine(PracticeSong());
    }
    
    public void OnSourceConnected(MIDISourceEntity se)
    {
        se.OnNotePressed += HandleNotePressed;
        se.OnNoteReleased += HandleNoteReleased;
    }

    public void OnSourceDisconnected(MIDISourceEntity se)
    {
        se.OnNotePressed -= HandleNotePressed;
        se.OnNoteReleased -= HandleNoteReleased;
    }

    private void HandleNotePressed(MIDINoteOnMessage noteOnMsg)
    {
        m_keyDowns.Add(noteOnMsg.Pitch);
    }

    private void HandleNoteReleased(MIDINoteOffMessage noteOffMsg)
    {
        m_keyDowns.Remove(noteOffMsg.Pitch);
    }

    private IEnumerator PracticeSong()
    {
        while (true)
        {
            if (m_scrollView.ScrollPosition != m_currentPosition)
            {
                m_currentPosition = m_scrollView.ScrollPosition;
            }

            bool allDown = true;
            List<MIDINote> currentNotes = m_songSpawner.LoadedSequenceStepper.GetMessagesForCurrentPosition();
            foreach (MIDINote mn in currentNotes)
            {
                allDown &= m_keyDowns.Contains(mn.Pitch);
            }

            if (allDown)
            {
                m_currentPosition += m_scrollSpeed * Time.deltaTime;
                m_scrollView.ScrollPosition = m_currentPosition;
                // Wait for Key Pressed
            }

            yield return null;
        }
    }
}
