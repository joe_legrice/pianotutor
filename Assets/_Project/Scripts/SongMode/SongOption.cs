﻿using UnityEngine;
using System;

public class SongOption : MonoBehaviour
{
    [SerializeField] private tk2dUIItem m_uiItem;
    [SerializeField] private tk2dTextMesh m_songTitleTextMesh;

    private Song m_song;

    public void Setup(Song s, Action<Song> callback)
    {
        m_song = s;
        m_songTitleTextMesh.text = s.SongName;
        m_uiItem.OnClick += () => {
            callback(m_song);
        };
    }
}
