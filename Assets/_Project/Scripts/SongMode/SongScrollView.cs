﻿using UnityEngine;
using System.Collections;

public class SongScrollView : MonoBehaviour
{
    [SerializeField] private Camera m_camera;
    [SerializeField] private SongSpawner m_songSpawner;
    [SerializeField] private tk2dUIItem m_uiItem;
    [SerializeField] private Transform m_contentTransform;
    [SerializeField] private float m_lerpTime = 0.2f;

    private float m_position;
    private Vector2 m_lastTouchWorldPosition;
    private bool m_dragging;

    public float ScrollPosition
    {
        get { return m_position; }
        set
        {
            if (value != m_position)
            {
                m_position = Mathf.Clamp01(value);
                //m_songSpawner.LoadedSequenceStepper.SetTrackPosition(m_position);

                Vector3 newPosition = m_contentTransform.localPosition;
                newPosition.y = -m_position * m_songSpawner.LoadedSequenceStepper.NumberOfTicks * m_songSpawner.UnitsPerTick;
                m_contentTransform.localPosition = newPosition;

                m_songSpawner.LoadedSequenceStepper.SetTrackPosition((int)(m_position * m_songSpawner.LoadedSequenceStepper.NumberOfTicks));
            }
        }
    }

    private void Start()
    {
        m_uiItem.OnDown += DragStarted;
        m_uiItem.OnUp += DragEnded;
    }

    private void OnDisable()
    {
        m_dragging = false;
    }

    private void Update()
    {
        if (m_dragging)
        {
            Vector2 touchPos = m_camera.ScreenToWorldPoint(m_uiItem.Touch.position);
            Vector2 touchDiff = touchPos - m_lastTouchWorldPosition;
            Vector3 newPosition = m_contentTransform.localPosition;

            float maxPosition = m_songSpawner.LoadedSequenceStepper.NumberOfTicks * m_songSpawner.UnitsPerTick;
            newPosition.y = Mathf.Clamp(newPosition.y + touchDiff.y, -maxPosition, 0);

            m_contentTransform.localPosition = newPosition;
            m_lastTouchWorldPosition = touchPos;
        }
    }
    
    private void DragStarted()
    {
        m_dragging = true;
        StopAllCoroutines();
        m_lastTouchWorldPosition = m_camera.ScreenToWorldPoint(m_uiItem.Touch.position);
    }

    private void DragEnded()
    {
        m_dragging = false;
        StartCoroutine(ClipToNearest());
    }

    private IEnumerator ClipToNearest()
    {
        int currentTick = Mathf.RoundToInt(m_contentTransform.localPosition.y / m_songSpawner.UnitsPerTick);
        int ticksPerQuarter = m_songSpawner.LoadedSequenceStepper.TicksPerBar / 4;
        int roundedTick = ticksPerQuarter * ((currentTick / ticksPerQuarter) + Mathf.RoundToInt((float)(currentTick % ticksPerQuarter) / ticksPerQuarter));

        float time = 0.0f;
        float initialPosition = m_contentTransform.localPosition.y;
        float targetPosition = roundedTick * m_songSpawner.UnitsPerTick;

        m_position = roundedTick / m_songSpawner.LoadedSequenceStepper.NumberOfTicks;
        m_songSpawner.LoadedSequenceStepper.SetTrackPosition(roundedTick);

        while (time < m_lerpTime)
        {
            Vector3 newPosition = m_contentTransform.localPosition;
            newPosition.y = Mathf.Lerp(initialPosition, targetPosition, time / m_lerpTime);
            m_contentTransform.localPosition = newPosition;

            yield return null;
            time += Time.deltaTime;
        }

        Vector3 finalPosition = m_contentTransform.localPosition;
        finalPosition.y = targetPosition;
        m_contentTransform.localPosition = finalPosition;
    }
}
