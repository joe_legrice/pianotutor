﻿using UnityEngine;

public class Song : ScriptableObject
{
    [SerializeField] private string m_songName;
    [SerializeField] [StreamingAssetPath] private string m_fileName;

    public string SongName { get { return m_songName; } }
    public string SongPath { get { return Application.streamingAssetsPath + "/" + m_fileName; } }
}
