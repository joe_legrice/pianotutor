﻿using UnityEngine;
using UnityEditor;
using UnityMidiKit;

[CustomEditor(typeof(PianoController))]
public class PianoControllerInspector : Editor
{
    private Pitch m_initialPitch = Pitch.C3;
    private int m_numberOfOctaves = 1;

    private PianoController Target { get { return (PianoController)target; } }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("Initialise Piano Layout");
        m_initialPitch = (Pitch)EditorGUILayout.EnumPopup("Initial Pitch", m_initialPitch);
        m_numberOfOctaves = Mathf.Max(EditorGUILayout.IntField("Number of Octaves", m_numberOfOctaves), 1);
        if (GUILayout.Button("Setup Piano"))
        {
            Target.RefreshKeys(m_initialPitch, m_initialPitch.ShiftOctave(m_numberOfOctaves) - 1);
        }
        if (GUILayout.Button("Clear Layout"))
        {
            Target.ClearAllKeys();
        }
    }
}
