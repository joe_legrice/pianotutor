﻿using UnityEngine;
using UnityMidiKit;

public class PianoKey : MonoBehaviour, ISourceListener
{
    [SerializeField] private bool m_isBlackKey;
    [SerializeField] private Vector2 m_size;
    [SerializeField] private GameObject m_releasedHierarchy;
    [SerializeField] private GameObject m_pressedHierarchy;
    [SerializeField] private Transform m_tutorialTarget;

    private Transform m_transform;

    public Pitch Pitch { get; private set; }
    
    public void Setup(Pitch p)
    {
        Pitch = p;
    }

    public Vector2 GetSize()
    {
        return m_size;
    }

    public Vector3 GetPosition()
    {
        return m_transform.position;
    }

    public Transform GetTutorialTargetTransform()
    {
        return m_tutorialTarget;
    }

    public void OnSourceConnected(MIDISourceEntity se)
    {
        se.OnNotePressed += NotePressed;
        se.OnNoteReleased += NoteReleased;
    }

    public void OnSourceDisconnected(MIDISourceEntity se)
    {
        se.OnNotePressed -= NotePressed;
        se.OnNoteReleased -= NoteReleased;
    }

    private void Awake()
    {
        m_transform = GetComponent<Transform>();
        DeviceManager.Instance.RegisterDeviceListener(this);

        m_releasedHierarchy.SetActive(true);
        m_pressedHierarchy.SetActive(false);
    }

    private void OnDestroy()
    {
        DeviceManager.Instance.RemoveDeviceListener(this);
    }

    private void NotePressed(MIDINoteOnMessage msg)
    {
        if (msg.Pitch == Pitch)
        {
            m_releasedHierarchy.SetActive(false);
            m_pressedHierarchy.SetActive(true);
        }
    }

    private void NoteReleased(MIDINoteOffMessage msg)
    {
        if (msg.Pitch == Pitch)
        {
            m_releasedHierarchy.SetActive(true);
            m_pressedHierarchy.SetActive(false);
        }
    }
}
