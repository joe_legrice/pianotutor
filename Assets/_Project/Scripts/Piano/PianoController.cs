﻿using UnityEngine;
using UnityMidiKit;

using System.Collections;
using System.Collections.Generic;

public class PianoController : MonoBehaviour
{
    [SerializeField] private GameObject m_whiteKeyPrefab;
    [SerializeField] private GameObject m_blackKeyPrefab;
    [SerializeField] private Transform m_parentTransform;
    [SerializeField] [HideInInspector] private List<PianoKey> m_keys;

    private Pitch m_minPitch;
    private Pitch m_maxPitch;

    public void RefreshKeys(Pitch minPitch, Pitch maxPitch)
    {
        ClearAllKeys();

        m_minPitch = minPitch;
        m_maxPitch = maxPitch;

        int numberOfWhiteKeys = 0;
        for (int pitchIndex = (int)m_minPitch; pitchIndex < (int)m_maxPitch; pitchIndex++)
        {
            if (!((Pitch)pitchIndex).IsSharp())
            {
                numberOfWhiteKeys++;
            }
        }
        
        Vector2 whiteKeySize = m_whiteKeyPrefab.GetComponent<PianoKey>().GetSize();

        float xPosition = -0.5f * numberOfWhiteKeys * whiteKeySize.x;
        for (int pitchIndex = (int)minPitch; pitchIndex <= (int)maxPitch; pitchIndex++)
        {
            Pitch p = (Pitch)pitchIndex;
            GameObject keyObject = null;
            bool whiteKey = !p.IsSharp();
            if (whiteKey)
            {
                keyObject = Instantiate(m_whiteKeyPrefab);
            }
            else
            {
                xPosition -= 0.5f * whiteKeySize.x;
                keyObject = Instantiate(m_blackKeyPrefab);
            }

            Transform t = keyObject.GetComponent<Transform>();
            t.parent = m_parentTransform;
            t.localScale = Vector3.one;
            t.localPosition = new Vector3(xPosition, 0, 0);

            PianoKey pk = keyObject.GetComponent<PianoKey>();
            pk.Setup(p);
            m_keys.Add(pk);

            xPosition += whiteKey ? whiteKeySize.x : 0.5f * whiteKeySize.x;
        }
    }

    public void ClearAllKeys()
    {
        if (m_keys != null)
        {
            foreach (PianoKey pk in m_keys)
            {
                if (pk != null && pk.gameObject != null)
                {
                    DestroyImmediate(pk.gameObject);
                }
            }
        }
        m_keys = new List<PianoKey>();
    }

    public PianoKey GetKey(Pitch p)
    {
        int index = Mathf.Clamp((int)p - (int)m_minPitch, 0, m_keys.Count - 1);
        return m_keys[index];
    }
}
