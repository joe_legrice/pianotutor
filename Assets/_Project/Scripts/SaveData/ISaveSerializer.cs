﻿
public interface ISaveSerializer
{
    string SerializeToString();
    void RestoreFromString(string s);
}
