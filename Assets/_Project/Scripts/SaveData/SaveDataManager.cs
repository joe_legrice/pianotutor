﻿using UnityEngine;
using Newtonsoft.Json;

using System.IO;
using System.Text;
using System.Collections.Generic;

public class SaveDataManager : Singleton<SaveDataManager>
{
    private Dictionary<string, string> m_saveData;
    private static string SaveDataFileName { get { return Application.persistentDataPath + "/datacache.dat"; } }

    public void SaveCacheToDisk()
    {
        string saveData = JsonConvert.SerializeObject(m_saveData);
        byte[] bytes = Encoding.UTF8.GetBytes(saveData);
        File.WriteAllBytes(SaveDataFileName, bytes);
    }

    public void CacheLoadDataFromDisk()
    {
        if (File.Exists(SaveDataFileName))
        {
            byte[] bytes = File.ReadAllBytes(SaveDataFileName);
            string saveData = Encoding.UTF8.GetString(bytes);
            m_saveData = JsonConvert.DeserializeObject<Dictionary<string, string>>(saveData);
        }
        else
        {
            m_saveData = new Dictionary<string, string>();
        }
    }

    public bool SaveDataExistsForKey(string saveDataKey)
    {
        return m_saveData.ContainsKey(saveDataKey);
    }

    public void SaveObjectForKey(string saveDataKey, ISaveSerializer obj)
    {
        m_saveData[saveDataKey] = obj.SerializeToString();
        SaveCacheToDisk();
    }

    public string GetStringForKey(string saveDataKey)
    {
        string result = null;
        m_saveData.TryGetValue(saveDataKey, out result);
        return result;
    }
}
