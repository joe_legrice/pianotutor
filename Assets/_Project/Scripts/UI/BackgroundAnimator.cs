﻿using UnityEngine;
using System.Collections;

public class BackgroundAnimator : MonoBehaviour
{
    [SerializeField] private MeshRenderer m_meshRenderer;
    [SerializeField] private float m_animateTime;
    [SerializeField] private float m_minValue;
    [SerializeField] private float m_maxValue;

    private float m_time;

    private void Update ()
    {
        m_time += Time.deltaTime;

        float percent = (m_time % m_animateTime) / m_animateTime;
        int dir = (int)((m_time / m_animateTime) % 2);
        float progress = (1 - dir) * percent + dir * (1.0f-percent);
        m_meshRenderer.material.SetFloat("_Factor", Mathf.Lerp(m_minValue, m_maxValue, progress));
    }
}
