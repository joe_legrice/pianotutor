﻿using UnityEngine;
using UnityMidiKit;

public class DescendingNote : MonoBehaviour
{
    [SerializeField] private tk2dSlicedSprite m_slicedSprite;

    public Pitch Pitch { get; private set; }
    public int StartTick { get; private set; }
    public int EndTick { get; private set; }

    private float m_unitsPerTick;

    public void Setup(MIDINote mn, PianoKey pianoKey, float unitsPerTick)
    {
        Pitch = mn.Pitch;
        StartTick = mn.StartTick;
        EndTick = mn.ReleaseTick;
        m_unitsPerTick = unitsPerTick;
        
        Transform t = GetComponent<Transform>();
        t.localPosition = new Vector3() {
            x = pianoKey.GetPosition().x,
            y = m_unitsPerTick * StartTick
        };

        m_slicedSprite.dimensions = new Vector2() {
            x = m_slicedSprite.dimensions.x,
            y = m_unitsPerTick * (EndTick - StartTick)
        };
    }
}
