﻿using UnityEngine;
using UnityMidiKit;

public class AscendingNote : MonoBehaviour
{
    [SerializeField] private tk2dSlicedSprite m_slicedSprite;
    [SerializeField] private float m_movementSpeed;

    private bool m_sustained;
    private PianoKey m_pianoKey;
    private Transform m_transform;
    
    public void SetNoteOn(PianoKey pianoKey)
    {
        m_sustained = true;

        m_pianoKey = pianoKey;
        Vector3 keyPosition = m_pianoKey.GetPosition();
        m_slicedSprite.dimensions = new Vector2(m_pianoKey.GetSize().x, 0.5f * m_pianoKey.GetSize().y);

        m_transform = GetComponent<Transform>();
        m_transform.position = keyPosition;
    }

    public void ReleaseNote()
    {
        m_sustained = false;
    }
    
    private void Update()
    {
        if (m_pianoKey != null)
        {
            if (m_sustained)
            {
                Vector2 newDimensions = m_slicedSprite.dimensions;
                newDimensions.y += m_movementSpeed * Time.deltaTime;
                m_slicedSprite.dimensions = newDimensions;
            }
            else
            {
                Vector3 newPosition = m_transform.position;
                newPosition.y += m_movementSpeed * Time.deltaTime;
                m_transform.position = newPosition;
            }
        }
    }
}
