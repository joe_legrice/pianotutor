﻿using UnityEngine;
using UnityMidiKit;

public class BarLine : MonoBehaviour
{
	[SerializeField] private GameObject m_quaterNotePrefab;
	[SerializeField] private GameObject m_eighthNotePrefab;
	[SerializeField] private Transform m_parentTransform;

    public void Initialise(int bar, int ticksPerQuarterNote, float unitsPerTick)
    {
        Transform thisTransform = GetComponent<Transform>();
        thisTransform.localPosition = new Vector3() {
            y = unitsPerTick * bar * ticksPerQuarterNote * 4
        };

        for (int quaterNoteIndex = 0; quaterNoteIndex < 4; quaterNoteIndex++)
        {
            if (quaterNoteIndex > 0)
            {
                GameObject quaterNoteObject = Instantiate(m_quaterNotePrefab);
                Transform quaterNoteObjectTransform = quaterNoteObject.GetComponent<Transform>();
                quaterNoteObjectTransform.parent = m_parentTransform;
                quaterNoteObjectTransform.localPosition = new Vector3() {
                    y = (quaterNoteIndex) * unitsPerTick * ticksPerQuarterNote
                };
            }

            GameObject eighthNoteObject = Instantiate(m_eighthNotePrefab);
            Transform eightNoteTransform = eighthNoteObject.GetComponent<Transform>();
            eightNoteTransform.parent = m_parentTransform;
            eightNoteTransform.localPosition = new Vector3() {
                y = (0.5f + quaterNoteIndex) * unitsPerTick * ticksPerQuarterNote
            };
        }
    }
}
