﻿using UnityEngine;
using UnityMidiKit;

public class NoteDot : MonoBehaviour
{
    [SerializeField] private Transform m_dotTransform;
    [SerializeField] private PianoController m_pianoController;

    public void SetNoteTarget(Pitch p)
    {
        m_dotTransform.parent = m_pianoController.GetKey(p).GetTutorialTargetTransform();
        m_dotTransform.localPosition = Vector3.zero;
    }
}
