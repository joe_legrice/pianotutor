﻿using UnityEngine;
using UnityMidiKit;
using System.Collections;

public class PianoScroller : MonoBehaviour
{
    [SerializeField] private Transform m_pianoNoteParentTransform;
    [SerializeField] private PianoController m_pianoController;
    [SerializeField] private AnimationCurve m_animationCurve;
    [SerializeField] private float m_scrollTime;

    public void SetCentrePitch(Pitch p)
    {
        StopAllCoroutines();
        StartCoroutine(ScrollToTargetPitch(p));
    }

    private IEnumerator ScrollToTargetPitch(Pitch p)
    {
        Pitch targetPitch = p;

        Vector3 initialPosition = m_pianoNoteParentTransform.localPosition;
        Vector3 targetPosition = m_pianoNoteParentTransform.InverseTransformPoint(m_pianoController.GetKey(p).GetPosition());

        float timeTaken = 0.0f;
        while (timeTaken / m_scrollTime < 1.0f)
        {
            m_pianoNoteParentTransform.localPosition = Vector3.Lerp(initialPosition, targetPosition, m_animationCurve.Evaluate(timeTaken / m_scrollTime));
            timeTaken += Time.deltaTime;
            yield return null;
        }
        m_pianoNoteParentTransform.localPosition = targetPosition;
    }
}
