﻿using UnityEngine;
using UnityMidiKit;
using System.Collections.Generic;

public class ScalePractice : MonoBehaviour, ISourceListener
{
    [SerializeField] private PianoScroller m_pianoScroller;
    [SerializeField] private NoteDot m_noteDot;
    [SerializeField] private ScaleDefinition m_scaleDefinition;

    private int m_currentRoundIndex;
    private List<PracticeSessionRound> m_practiceSessionRounds;

    private PracticeSessionRound CurrentRound { get { return m_practiceSessionRounds[m_currentRoundIndex]; } }

    private void Start()
    {
        DeviceManager.Instance.RegisterDeviceListener(this);
        StartPracticing(PracticeSessionGenerator.GeneratePracticeSession(m_scaleDefinition));
    }

    public void StartPracticing(List<PracticeSessionRound> practiceRounds)
    {
        m_currentRoundIndex = 0;
        m_practiceSessionRounds = practiceRounds;
        m_noteDot.SetNoteTarget(CurrentRound.GetNextPitch());
        m_pianoScroller.SetCentrePitch(CurrentRound.GetCentrePitch());
    }

    public void OnSourceConnected(MIDISourceEntity se)
    {
        se.OnNotePressed += OnNotePressed;
        se.OnNoteReleased += OnNoteReleased;
    }

    public void OnSourceDisconnected(MIDISourceEntity se)
    {
        se.OnNotePressed -= OnNotePressed;
        se.OnNoteReleased -= OnNoteReleased;
    }

    private void OnNotePressed(MIDINoteOnMessage obj)
    {
        CurrentRound.KeyPressed(obj.Pitch);
        ContinueSession();
    }

    private void OnNoteReleased(MIDINoteOffMessage obj)
    {
        CurrentRound.KeyReleased(obj.Pitch);
        ContinueSession();
    }

    private void ContinueSession()
    {
        if (CurrentRound.IsRoundComplete())
        {
            m_currentRoundIndex++;
            if (m_currentRoundIndex > m_practiceSessionRounds.Count - 1)
            {
                m_currentRoundIndex = 0; // Start again for now.
            }
        }
        m_noteDot.SetNoteTarget(CurrentRound.GetNextPitch());
    }
}
