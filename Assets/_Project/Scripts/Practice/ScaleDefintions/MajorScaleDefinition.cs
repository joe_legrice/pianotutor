﻿using UnityMidiKit;
using System;
using System.Collections.Generic;

public class MajorScaleDefinition : ScaleDefinition
{
    private readonly int[] c_semitoneProgression = {
        2,
        2,
        1,
        2,
        2,
        2,
        1
    };

    public override int NumberOfPitchesInScale()
    {
        return c_semitoneProgression.Length + 1;
    }

    public override string GetScaleName()
    {
        return "Major Scale";
    }

    public override List<Pitch> GetScale(Pitch rootPitch)
    {
        List<Pitch> scale = new List<Pitch>();
        for (int scalePitchIndex = 0; scalePitchIndex <= c_semitoneProgression.Length; scalePitchIndex++)
        {
            int pitchIndex = (int)rootPitch;
            for (int i = 0; i < scalePitchIndex; i++)
            {
                pitchIndex += c_semitoneProgression[i];
            }

            if (pitchIndex < Enum.GetNames(typeof(Pitch)).Length)
            {
                Pitch p = (Pitch)pitchIndex;
                scale.Add(p);
            }
            else
            {
                break;
            }
        }
        return scale;
    }
}
