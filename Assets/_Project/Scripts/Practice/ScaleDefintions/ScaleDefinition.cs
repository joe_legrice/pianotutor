﻿using UnityEngine;
using UnityMidiKit;
using System.Collections.Generic;

public abstract class ScaleDefinition : MonoBehaviour
{
    private static ScaleDefinition[] s_instances;
    public static ScaleDefinition[] ActiveDefinitions
    {
        get
        {
            if (s_instances == null)
            {
                s_instances = FindObjectsOfType<ScaleDefinition>();
            }
            return s_instances;
        }
    }

    public abstract int NumberOfPitchesInScale();
    public abstract string GetScaleName();
    public abstract List<Pitch> GetScale(Pitch rootPitch);
}
