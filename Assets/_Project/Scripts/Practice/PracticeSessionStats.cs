﻿using UnityMidiKit;
using System.Collections.Generic;

public partial class PracticeSessionStats : Singleton<PracticeSessionStats>, ISaveSerializer
{
    private Dictionary<int, List<StatEntry>> m_statistics = new Dictionary<int, List<StatEntry>>();

    public void AddRound(PracticeSessionRound psr)
    {
        if (psr.IsRoundComplete())
        {
            int pitchIndex = psr.GetRootNote().GetNoteIndex();
            List<StatEntry> entries = null;
            if (!m_statistics.TryGetValue(pitchIndex, out entries))
            {
                entries = new List<StatEntry>();
            }
            entries.Add(new StatEntry(psr));
            m_statistics[pitchIndex] = entries;
        }
    }

    public float GetAverageAccuracy(Pitch rootNote, ScaleDefinition sd,
        PracticeSessionRound.Hand? hand = null, PracticeSessionRound.PlayStyle? playStyle = null)
    {
        List<StatEntry> entries = GetStatEntries(rootNote, sd, hand, playStyle);
        if (entries != null)
        {
            float total = 0.0f;
            foreach (StatEntry se in entries)
            {
                total += se.GetAccuracy();
            }
            return total / entries.Count;
        }
        else
        {
            return 0.0f;
        }
    }

    public float GetAverageTimingVarience(Pitch rootNote, ScaleDefinition sd,
        PracticeSessionRound.Hand? hand = null, PracticeSessionRound.PlayStyle? playStyle = null)
    {
        List<StatEntry> entries = GetStatEntries(rootNote, sd, hand, playStyle);
        if (entries != null)
        {
            float total = 0.0f;
            foreach (StatEntry se in entries)
            {
                total += se.GetTimingVarience();
            }
            return total / entries.Count;
        }
        else
        {
            return 0.0f;
        }
    }

    private List<StatEntry> GetStatEntries(Pitch rootNote, ScaleDefinition sd,
        PracticeSessionRound.Hand? hand = null, PracticeSessionRound.PlayStyle? playStyle = null)
    {
        int pitchIndex = rootNote.GetNoteIndex();
        List<StatEntry> entries = null;
        if (m_statistics.TryGetValue(pitchIndex, out entries))
        {
            List<StatEntry> result = new List<StatEntry>();
            foreach (StatEntry se in entries)
            {
                if (se.ScaleDefinitionName == sd.GetScaleName() && se.Hand == hand &&
                    (hand != null && hand.Value == se.Hand || hand == null) &&
                    (playStyle != null && playStyle.Value == se.PlayStyle || playStyle == null))
                {
                    result.Add(se);
                }
            }
            return result;
        }
        else
        {
            return null;
        }
    }
}
