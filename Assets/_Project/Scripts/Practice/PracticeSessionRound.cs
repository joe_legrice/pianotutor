﻿using UnityEngine;
using UnityMidiKit;
using System.Collections.Generic;

public class PracticeSessionRound
{
    public enum Hand
    {
        LeftHand = 0,
        RightHand = 1
    }

    public enum PlayStyle
    {
        Any = 0,
        Legato = 1,
        Staccato = 2
    }

    private Hand m_hand;
    private PlayStyle m_style;

    private int m_mistakes;
    private int m_progress;
    private Pitch m_rootNote;
    private List<List<float>> m_keyPressTimes;
    private List<Pitch> m_pitches;
    private ScaleDefinition m_scaleDefinition;

    public PracticeSessionRound(ScaleDefinition sd, Pitch rootNote, Hand practiceHand, PlayStyle playStyle)
    {
        m_hand = practiceHand;
        m_style = playStyle;

        m_scaleDefinition = sd;
        m_rootNote = rootNote;
        m_pitches = sd.GetScale(rootNote);

        m_keyPressTimes = new List<List<float>>();
        m_keyPressTimes.Add(new List<float>());
    }

    public Pitch GetRootNote()
    {
        return m_rootNote;
    }

    public Hand GetHand()
    {
        return m_hand;
    }

    public ScaleDefinition GetScaleDefinition()
    {
        return m_scaleDefinition;
    }

    public int GetNumberOfMistakesMade()
    {
        return m_mistakes;
    }
    
    public PlayStyle GetPlayStyle()
    {
        return m_style;
    }

    public List<List<float>> GetKeyPressTimesPerAttempt()
    {
        return m_keyPressTimes;
    }

    public int GetProgress()
    {
        return m_progress;
    }

    public Pitch GetNextPitch()
    {
        return m_pitches[m_progress];
    }

    public bool IsRoundComplete()
    {
        return m_progress == m_pitches.Count;
    }

    public Pitch GetCentrePitch()
    {
        Pitch lowestPitch = Pitch.G9;
        Pitch highestPitch = Pitch.CNeg1;
        foreach (Pitch p in m_pitches)
        {
            if (p < lowestPitch)
            {
                lowestPitch = p;
            }

            if (p > highestPitch)
            {
                highestPitch = p;
            }
        }

        return lowestPitch + (highestPitch - lowestPitch) / 2;
    }

    public void KeyPressed(Pitch p)
    {
        if (!IsRoundComplete() && m_style == PlayStyle.Any)
        {
            m_keyPressTimes[m_mistakes].Add(Time.time);

            bool pitchCorrect = p.GetNoteIndex() == m_pitches[m_progress].GetNoteIndex();
            bool playStyleCorrect = m_style == PlayStyle.Any;
            if (pitchCorrect && playStyleCorrect)
            {
                m_progress++;
            }
            else
            {
                m_mistakes++;
                m_progress = 0;
                m_keyPressTimes.Add(new List<float>());
            }
        }
    }
    
    public void KeyReleased(Pitch p)
    {
        if (m_style == PlayStyle.Legato)
        {
        }
        else if (m_style == PlayStyle.Staccato)
        {
        }
    }
}

