﻿using UnityEngine;
using UnityMidiKit;

using System.Collections.Generic;

public class ProgressRow : MonoBehaviour
{
    [SerializeField] private tk2dTextMesh m_rootNoteTextMesh;
    [SerializeField] private tk2dTextMesh m_leftHandTimingTextMesh;
    [SerializeField] private tk2dTextMesh m_leftHandAccuracyTextMesh;
    [SerializeField] private tk2dTextMesh m_rightHandTimingTextMesh;
    [SerializeField] private tk2dTextMesh m_rightHandAccuracyTextMesh;

    public void Setup(Pitch rootNote, List<StatEntry> entries)
    {
        m_rootNoteTextMesh.text = rootNote.GetName();

        int leftHandEntries = 0;
        float leftHandTime = 0.0f;
        float leftHandAccuracy = 0.0f;

        int rightHandEntries = 0;
        float rightHandTime = 0.0f;
        float rightHandAccuracy = 0.0f;
        foreach (StatEntry se in entries)
        {
            if (se.Hand == PracticeSessionRound.Hand.LeftHand)
            {
                leftHandEntries++;
                leftHandTime += se.GetTimingVarience();
                leftHandAccuracy += se.GetAccuracy();
            }
            else
            {
                rightHandEntries++;
                rightHandTime += se.GetTimingVarience();
                rightHandAccuracy += se.GetAccuracy();
            }
        }

        if (leftHandEntries > 0)
        {
            m_leftHandTimingTextMesh.text = (leftHandTime / leftHandEntries).ToString();
            m_leftHandAccuracyTextMesh.text = (leftHandAccuracy / leftHandEntries).ToString();
        }
        else
        {
            m_leftHandTimingTextMesh.text = "";
            m_leftHandAccuracyTextMesh.text = "";
        }

        if (rightHandEntries > 0)
        {
            m_rightHandTimingTextMesh.text = (rightHandTime / rightHandEntries).ToString();
            m_rightHandAccuracyTextMesh.text = (rightHandAccuracy / rightHandEntries).ToString();
        }
        else
        {
            m_rightHandTimingTextMesh.text = "";
            m_rightHandAccuracyTextMesh.text = "";
        }
    }
}
