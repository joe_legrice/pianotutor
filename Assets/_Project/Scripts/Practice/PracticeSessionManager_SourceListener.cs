﻿using UnityEngine;
using UnityMidiKit;

public partial class PracticeSessionManager : MonoBehaviour, ISourceListener
{
    private void Awake()
    {
        DeviceManager.Instance.RegisterDeviceListener(this);
    }

    public void OnSourceConnected(MIDISourceEntity se)
    {
        se.OnNotePressed += OnNotePressed;
        se.OnNoteReleased += OnNoteReleased;
    }

    public void OnSourceDisconnected(MIDISourceEntity se)
    {
        se.OnNotePressed -= OnNotePressed;
        se.OnNoteReleased -= OnNoteReleased;
    }

    private void OnNotePressed(MIDINoteOnMessage obj)
    {
        if (m_currentState == PracticeSessionState.InSession)
        {
            PracticeSessionRound psr = m_rounds[m_currentPracticeRound];
            psr.KeyPressed(obj.Pitch);
            ContinueSession();
        }
    }

    private void OnNoteReleased(MIDINoteOffMessage obj)
    {
        if (m_currentState == PracticeSessionState.InSession)
        {
            PracticeSessionRound psr = m_rounds[m_currentPracticeRound];
            psr.KeyReleased(obj.Pitch);
            ContinueSession();
        }
    }
}
