﻿using UnityEngine;
using UnityMidiKit;
using System.Collections.Generic;

public partial class PracticeSessionManager : MonoBehaviour, ISourceListener
{
    [SerializeField] private ScrollViewSpawner m_spawner;

    [SerializeField] private GameObject m_notPracticingHierarchy;
    [SerializeField] private GameObject m_practiceSelectHierarchy;
    [SerializeField] private GameObject m_inProgressHierarchy;
    [SerializeField] private GameObject m_finishedHierarchy;

    [SerializeField] private tk2dUIItem m_selectSessionButton;
    [SerializeField] private tk2dUIItem m_finishSessionButton;
    
    [SerializeField] private PracticeSessionSummary m_sessionSummary;
    [SerializeField] private tk2dTextMesh m_scaleTitleTextMesh;
    [SerializeField] private tk2dTextMesh m_rootNoteTextMesh;
    [SerializeField] private ProgressMarkerParent m_progressMarkerControl;
    [SerializeField] private NoteSpawner m_noteSpawner;

    private enum PracticeSessionState
    {
        None,
        SelectingSession,
        InSession,
        SessionComplete
    };

    private int m_currentPracticeRound;
    private ScaleDefinition m_currentScaleDefinition;
    private PracticeSessionState m_currentState;
    private List<PracticeSessionRound> m_rounds;
    private List<PracticeOption> m_options;

    private void Start()
    {
        m_selectSessionButton.OnClick += SelectSession;
        m_finishSessionButton.OnClick += FinishSession;
    }

    private void Update()
    {
        m_notPracticingHierarchy.SetActive(m_currentState == PracticeSessionState.None);
        m_practiceSelectHierarchy.SetActive(m_currentState == PracticeSessionState.SelectingSession);
        m_inProgressHierarchy.SetActive(m_currentState == PracticeSessionState.InSession);
        m_finishedHierarchy.SetActive(m_currentState == PracticeSessionState.SessionComplete);
    }

    public void SelectSession()
    {
        m_currentState = PracticeSessionState.SelectingSession;

        m_spawner.RefreshItems(ScaleDefinition.ActiveDefinitions.Length);
        m_options = m_spawner.GetComponentInObjects<PracticeOption>();
        for (int scaleDefinitionIndex = 0; scaleDefinitionIndex < ScaleDefinition.ActiveDefinitions.Length; scaleDefinitionIndex++)
        {
            PracticeOption po = m_options[scaleDefinitionIndex];
            ScaleDefinition sd = ScaleDefinition.ActiveDefinitions[scaleDefinitionIndex];
            po.Setup(sd, HandleSelection);
        }
    }

    public void PracticeScale(ScaleDefinition sd)
    {
        m_currentState = PracticeSessionState.InSession;
        m_noteSpawner.SetRunning(true);

        m_currentPracticeRound = 0;
        m_currentScaleDefinition = sd;
        m_rounds = PracticeSessionGenerator.GeneratePracticeSession(sd);

        m_scaleTitleTextMesh.text = m_currentScaleDefinition.GetScaleName();
        Pitch firstRootNote = m_rounds[m_currentPracticeRound].GetRootNote();
        m_rootNoteTextMesh.text = "Root Note: " + firstRootNote.GetName();
        m_progressMarkerControl.SetNumberOfMarkers(m_currentScaleDefinition.NumberOfPitchesInScale());
    }

    private void HandleSelection(ScaleDefinition sd)
    {
        PracticeScale(sd);
    }

    private void ContinueSession()
    {
        PracticeSessionRound psr = m_rounds[m_currentPracticeRound];
        if (psr.IsRoundComplete())
        {
            m_currentPracticeRound++;
            m_progressMarkerControl.ResetProgress();

            if (m_currentPracticeRound < m_rounds.Count)
            {
                psr = m_rounds[m_currentPracticeRound];
                m_rootNoteTextMesh.text = "Root Note: " + psr.GetRootNote().GetName();
            }
            else
            {
                foreach (PracticeSessionRound ps in m_rounds)
                {
                    PracticeSessionStats.Instance.AddRound(ps);
                }
                PracticeSessionStats.Instance.Save();
                m_sessionSummary.UpdateWithRounds(m_rounds);
                m_currentState = PracticeSessionState.SessionComplete;
            }
        }
        else
        {
            m_progressMarkerControl.SetProgress(psr.GetProgress());
        }
    }

    private void FinishSession()
    {
        m_currentState = PracticeSessionState.None;
    }
}
