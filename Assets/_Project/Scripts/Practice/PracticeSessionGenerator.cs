﻿using UnityEngine;
using UnityMidiKit;
using System.Collections.Generic;

public class PracticeSessionGenerator
{
    public static List<PracticeSessionRound> GeneratePracticeSession(ScaleDefinition sd)
    {
        List<PracticeSessionRound> practiceSession = new List<PracticeSessionRound>();
        for (int roundIndex = 0; roundIndex < 2; roundIndex++)
        {
            Pitch p = (Pitch)((int)Pitch.C0 + (roundIndex % 12));
            PracticeSessionRound.Hand hand = roundIndex / 12 == 0 ? PracticeSessionRound.Hand.LeftHand : PracticeSessionRound.Hand.RightHand;
            PracticeSessionRound.PlayStyle playStyle = PracticeSessionRound.PlayStyle.Any;
            practiceSession.Add(new PracticeSessionRound(sd, p, hand, playStyle));
        }
        return practiceSession;
    }
}
