﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProgressMarkerParent : MonoBehaviour
{
    [SerializeField] private GameObject m_progressMarkerPrefab;
    [SerializeField] private FixedLayout m_fixedLayout;
    [SerializeField] private float m_activationDelay = 0.2f;

    private int m_targetProgress;
    private int m_currentProgress;
    private float m_time;
    private List<HierarchyController> m_controllers = new List<HierarchyController>();

    public void SetNumberOfMarkers(int progressCount)
    {
        m_currentProgress = 0;
        foreach (HierarchyController pmc in m_controllers)
        {
            DestroyImmediate(pmc.gameObject);
        }
        m_controllers.Clear();

        Transform parentTransform = m_fixedLayout.GetComponent<Transform>();
        for (int progressIndex = 0; progressIndex < progressCount; progressIndex++)
        {
            GameObject progressInstance = Instantiate(m_progressMarkerPrefab);
            progressInstance.GetComponent<Transform>().parent = parentTransform;
            m_controllers.Add(progressInstance.GetComponent<HierarchyController>());
        }
        m_fixedLayout.SetNumberOfColumns(progressCount);
        m_fixedLayout.RepositionChildren();
    }

    public void ResetProgress()
    {
        m_targetProgress = 0;
        m_currentProgress = 0;
        foreach (HierarchyController pmc in m_controllers)
        {
            pmc.SetState(false);
        }
    }

    public void SetProgress(int progress)
    {
        m_targetProgress = progress;
    }

    private void Update()
    {
        if (m_time >= m_activationDelay)
        {
            m_time = 0.0f;
            
            HierarchyController pmc = m_controllers[m_currentProgress];
            pmc.SetState(m_targetProgress > m_currentProgress);
            m_currentProgress += Mathf.Clamp(m_targetProgress - m_currentProgress, -1, 1);
        }
        else
        {
            m_time += Time.deltaTime;
        }
    }
}
