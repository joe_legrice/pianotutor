﻿using UnityMidiKit;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class PracticeSessionStats : Singleton<PracticeSessionStats>, ISaveSerializer
{
    private static readonly string s_saveDataKey = "practiceSessionStats";

    public void Save()
    {
        SaveDataManager.Instance.SaveObjectForKey(s_saveDataKey, this);
    }

    public void Load()
    {
        if (SaveDataManager.Instance.SaveDataExistsForKey(s_saveDataKey))
        {
            string json = SaveDataManager.Instance.GetStringForKey(s_saveDataKey);
            if (!string.IsNullOrEmpty(json))
            {
                RestoreFromString(json);
            }
        }
    }

    public string SerializeToString()
    {
        return JsonConvert.SerializeObject(m_statistics);
    }

    public void RestoreFromString(string json)
    {
        m_statistics = new Dictionary<int, List<StatEntry>>();
        JsonConvert.PopulateObject(json, m_statistics);
    }
}
