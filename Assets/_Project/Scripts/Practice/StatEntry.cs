﻿using UnityEngine;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;

[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
public class StatEntry
{
    [JsonProperty] private string m_scaleDefinitionName;
    [JsonProperty] private PracticeSessionRound.Hand m_hand;
    [JsonProperty] private PracticeSessionRound.PlayStyle m_playStyle;
    [JsonProperty] private DateTime m_utcDate;
    [JsonProperty] private int m_mistakesMade;
    [JsonProperty] private List<List<float>> m_keyPressTimesPerAttempt;

    public int Mistakes { get { return m_mistakesMade; } }
    public string ScaleDefinitionName { get { return m_scaleDefinitionName; } }
    public PracticeSessionRound.Hand Hand { get { return m_hand; } }
    public PracticeSessionRound.PlayStyle PlayStyle { get { return m_playStyle; } }

    public StatEntry() { }

    public StatEntry(PracticeSessionRound psr)
    {
        m_hand = psr.GetHand();
        m_utcDate = DateTime.UtcNow;
        m_playStyle = psr.GetPlayStyle();
        m_mistakesMade = psr.GetNumberOfMistakesMade();
        m_scaleDefinitionName = psr.GetScaleDefinition().GetScaleName();
        m_keyPressTimesPerAttempt = psr.GetKeyPressTimesPerAttempt();
    }

    public float GetAccuracy()
    {
        int totalKeyPresses = 0;
        foreach (List<float> keyPressList in m_keyPressTimesPerAttempt)
        {
            totalKeyPresses += keyPressList.Count;
        }
        return (float)(totalKeyPresses - m_mistakesMade) / totalKeyPresses;
    }

    public float GetAverageTiming()
    {
        int numberOfKeyPresses = 0;
        float totalTiming = 0.0f;
        foreach (List<float> keyPressList in m_keyPressTimesPerAttempt)
        {
            foreach (float keyPressTiming in keyPressList)
            {
                totalTiming += keyPressList.Count;
            }
            numberOfKeyPresses += keyPressList.Count;
        }
        return totalTiming / numberOfKeyPresses;
    }

    public float GetTimingVarience()
    {
        float averageTiming = GetAverageTiming();

        int numberOfKeyPresses = 0;
        float squaredDeviation = 0.0f;
        foreach (List<float> keyPressList in m_keyPressTimesPerAttempt)
        {
            foreach (float keyPressTiming in keyPressList)
            {
                squaredDeviation += Mathf.Pow(keyPressTiming - averageTiming, 2);
            }
            numberOfKeyPresses += keyPressList.Count;
        }
        return squaredDeviation / numberOfKeyPresses;
    }
}
