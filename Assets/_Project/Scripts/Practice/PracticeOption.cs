﻿using UnityEngine;
using System;

public class PracticeOption : MonoBehaviour
{
    [SerializeField] private tk2dUIItem m_uiItem;
    [SerializeField] private tk2dTextMesh m_textMesh;

    private ScaleDefinition m_scaleDefinition;

    public void Setup(ScaleDefinition sd, Action<ScaleDefinition> callback)
    {
        m_textMesh.text = sd.GetScaleName();
        m_scaleDefinition = sd;

        m_uiItem.OnClick += () => {
            callback(m_scaleDefinition);
        };
    }
}
