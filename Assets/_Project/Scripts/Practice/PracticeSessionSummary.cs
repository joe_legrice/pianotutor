﻿using UnityEngine;
using UnityMidiKit;
using System.Collections.Generic;

public class PracticeSessionSummary : MonoBehaviour
{
    [SerializeField] private tk2dUIScrollableArea m_scrollArea;
    [SerializeField] private FixedLayout m_layout;
    [SerializeField] private GameObject m_statPrefab;

    public void UpdateWithRounds(List<PracticeSessionRound> practiceSessionRounds)
    {
        Dictionary<Pitch, List<StatEntry>> progressRowData = new Dictionary<Pitch, List<StatEntry>>();
        foreach (PracticeSessionRound psr in practiceSessionRounds)
        {
            List<StatEntry> stats = null;
            if (!progressRowData.TryGetValue(psr.GetRootNote(), out stats))
            {
                stats = new List<StatEntry>();
            }
            stats.Add(new StatEntry(psr));
            progressRowData[psr.GetRootNote()] = stats;

        }

        foreach (KeyValuePair<Pitch, List<StatEntry>> kvp in progressRowData)
        {
            GameObject progressRowInstance = Instantiate(m_statPrefab);
            progressRowInstance.GetComponent<Transform>().parent = m_layout.GetComponent<Transform>();
            ProgressRow pr = progressRowInstance.GetComponent<ProgressRow>();
            pr.Setup(kvp.Key, kvp.Value);
        }
        m_layout.RepositionChildren();
        m_scrollArea.ContentLength = m_scrollArea.MeasureContentLength();
    }
}
