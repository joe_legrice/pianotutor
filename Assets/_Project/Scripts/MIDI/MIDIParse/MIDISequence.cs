﻿using UnityEngine;

namespace UnityMidiKit
{
    public class MIDISequence
    {
        private MIDITrack[] m_tracks;
        
        public int TicksPerQuarterNote { get; private set; }
        public int NumberOfTracks { get; private set; }

        public MIDISequence(MIDIParser.HeaderChunk hc)
        {
            NumberOfTracks = hc.NumberOfTracks;
            if ((hc.DefaultDeltaTimeUnit & 0x8000) == 0)
            {
                TicksPerQuarterNote = hc.DefaultDeltaTimeUnit;
            }
            else
            {
                int deltaTimePerFrame = hc.DefaultDeltaTimeUnit & 0x00FF;
                int framesPerSecond = (hc.DefaultDeltaTimeUnit & 0x7F00) >> 8;
                MIDI.LogError("[MIDISequence] DefaultDeltaTimeUnit not supported");
            }

            m_tracks = new MIDITrack[NumberOfTracks];
            for (int trackIndex = 0; trackIndex < NumberOfTracks; trackIndex++)
            {
                m_tracks[trackIndex] = new MIDITrack();
            }
        }

        public MIDITrack GetTrack(int trackIndex)
        {
            if (trackIndex < m_tracks.Length)
            {
                return m_tracks[trackIndex];
            }
            else
            {
                return null;
            }
        }

        public int GetMaxTicks()
        {
            int maxTicks = 0;
            foreach (MIDITrack t in m_tracks)
            {
                maxTicks = Mathf.Max(maxTicks, t.Ticks);
            }
            return maxTicks;
        }
    }
}