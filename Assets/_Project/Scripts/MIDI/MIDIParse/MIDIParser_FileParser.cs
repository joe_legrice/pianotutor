﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace UnityMidiKit
{
    public static partial class MIDIParser
    {
        [StructLayout(LayoutKind.Sequential, Size = 14, Pack = 0, CharSet = CharSet.Ansi)]
        public struct HeaderChunk
        {
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 Name;

            [Endian(Endianness.BigEndian)]
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 Size;

            [Endian(Endianness.BigEndian)]
            [MarshalAs(UnmanagedType.U2)]
            public UInt16 FileFormat;

            [Endian(Endianness.BigEndian)]
            [MarshalAs(UnmanagedType.U2)]
            public UInt16 NumberOfTracks;

            [Endian(Endianness.BigEndian)]
            [MarshalAs(UnmanagedType.U2)]
            public  UInt16 DefaultDeltaTimeUnit;

            public override string ToString()
            {
                return "[HeaderChunk: Name=" + Name + ", Size=" + Size + ", FileFormat=" + FileFormat + ", NumberOfTracks=" + NumberOfTracks + ", DefaultDeltaTimeUnit=" + DefaultDeltaTimeUnit + "]";
            }
        }

        [StructLayout(LayoutKind.Sequential, Size = 8, Pack = 0, CharSet = CharSet.Ansi)]
        public struct TrackHeaderChunk
        {
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 Name;

            [Endian(Endianness.BigEndian)]
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 TrackSize;

            public bool IsValid()
            {
                return Name == 1802654797;
            }

            public override string ToString()
            {
                return "[TrackHeaderChunk: Name=" + Name + ", TrackSize=" + TrackSize + "]";
            }
        }

        public static MIDISequence ParseFile(string path)
        {
            MIDI.Log("Path: " + path);
            MIDISequence sequence = null;
            if (File.Exists(path))
            {
                int headerBufferSize = 14; // Marshal.SizeOf(typeof(HeaderChunk));
                byte[] headerBuffer = new byte[headerBufferSize];

                FileStream fs = File.Open(path, FileMode.Open);
                if (fs.Read(headerBuffer, 0, headerBufferSize) > 0)
                {
                    HeaderChunk hc = ByteHelper.BytesToStruct<HeaderChunk>(headerBuffer);
                    sequence = new MIDISequence(hc);
                    MIDI.Log(hc);

                    for (int trackIndex = 0; trackIndex < hc.NumberOfTracks; trackIndex++)
                    {
                        ReadTrack(fs, sequence.GetTrack(trackIndex));
                    }
                }
                fs.Close();
            }
            return sequence;
        }

        private static void ReadTrack(FileStream fs, MIDITrack track)
        {
            int trackHeaderSize = 8;
            byte[] trackHeaderBuffer = new byte[trackHeaderSize];
            int trackHeaderBytesRead = fs.Read(trackHeaderBuffer, 0, trackHeaderSize);
            
            TrackHeaderChunk thc = ByteHelper.BytesToStruct<TrackHeaderChunk>(trackHeaderBuffer);
            MIDI.Log(thc);
            
            byte[] trackByteBuffer = new byte[thc.TrackSize];
            int trackBytesRead = fs.Read(trackByteBuffer, 0, (int)thc.TrackSize);
            int totalTicks = 0;

            StringBuilder etSb = new StringBuilder();

            int trackByteIndex = 0;
            while (trackByteIndex < trackBytesRead)
            {
                int startIndex = trackByteIndex;
                int deltaTime = GetVariableLength(trackByteBuffer, ref trackByteIndex);
                totalTicks += deltaTime;

                #region DELTA TIME STRINGBUILDER DEBUG
                StringBuilder deltaTimeStringBuilder = new StringBuilder();
                deltaTimeStringBuilder.Append("[" + totalTicks + "] [");
                for (int i = startIndex; i < trackByteIndex; i++)
                {
                    deltaTimeStringBuilder.Append(trackByteBuffer[i].ToString("x2"));
                    if (i < trackByteIndex - 1)
                    {
                        deltaTimeStringBuilder.Append(" ");
                    }
                }
                deltaTimeStringBuilder.Append("]");
                #endregion

                if (trackByteIndex < trackBytesRead)
                {
                    if (IsSystemExclusive(trackByteBuffer[trackByteIndex]))
                    {
                        string result = ParseSystemExclusive(trackByteBuffer, ref trackByteIndex);
                        etSb.Append(result);
                    }

                    if (IsMetaEvent(trackByteBuffer[trackByteIndex]))
                    {
                        MIDIMetaEvent metaEvent = ParseMetaEvent(totalTicks, trackByteBuffer, ref trackByteIndex);
                        if (metaEvent != null)
                        {
                            #region META EVENT LOGGING DEBUG
                            etSb.Append(deltaTimeStringBuilder);
                            etSb.AppendLine(" Meta Event: " + metaEvent);
                            #endregion
                            track.AppendMetaEvent(metaEvent);
                        }
                    }
                    else
                    {
                        byte commandByte = trackByteBuffer[trackByteIndex];
                        int numberOfDataBytes = BytesToReadForStatus(commandByte);
                        if (numberOfDataBytes > 0)
                        {
                            trackByteIndex++;

                            int numberOfEvents = 1;

                            bool findingNumberOfEvents = true;
                            while (findingNumberOfEvents)
                            {
                                int index = trackByteIndex + numberOfEvents * numberOfDataBytes;
                                if (index < trackByteBuffer.Length)
                                {
                                    for (int i = 0; i < numberOfDataBytes; i++)
                                    {
                                        findingNumberOfEvents &= (trackByteBuffer[index + i] & 0x80) != 0x80; //TODO: SysEx events can interrupt?
                                    }

                                    if (findingNumberOfEvents)
                                    {
                                        numberOfEvents++;
                                    }
                                }
                                else
                                {
                                    findingNumberOfEvents = false;
                                }
                            }

                            byte[] dataBytes = new byte[numberOfEvents * numberOfDataBytes];
                            Array.Copy(trackByteBuffer, trackByteIndex, dataBytes, 0, numberOfEvents * numberOfDataBytes);
                            List<MIDIMessage> midiMessages = ParseMessages(totalTicks, commandByte, dataBytes);
                            track.AppendMessages(midiMessages);

                            #region MIDI MESSAGE LOGGING DEBUG
                            foreach (MIDIMessage midiMessage in midiMessages)
                            {
                                etSb.Append(deltaTimeStringBuilder.ToString());
                                etSb.Append(" MIDI Event: " + midiMessage);
                                etSb.AppendLine();
                            }
                            #endregion

                            trackByteIndex += numberOfEvents * numberOfDataBytes;
                        }
                        else
                        {
                            etSb.AppendLine("[Unknown MIDI Event (Command Byte: " + commandByte.ToString("x2") + ")]");
                        }
                    }
                }
            }
            track.Ticks = totalTicks;
            MIDI.Log(etSb);
        }

        private static int GetVariableLength(byte[] inputArray, ref int startIndex)
        {
            int shifts = 0;

            while (true)
            {
                byte varLengthByte = inputArray[startIndex + shifts];
                if ((varLengthByte & 0x80) == 0 && (startIndex + shifts) < (inputArray.Length))
                {
                    break;
                }
                shifts++;
            }

            int variableLength = 0;
            while(true)
            {
                byte varLengthByte = inputArray[startIndex];
                variableLength += (varLengthByte & 0x7f) << (shifts * 7);
                shifts--;
                startIndex++;
                if ((varLengthByte & 0x80) == 0)
                {
                    break;
                }
            }
            return variableLength;
        }
    }
}