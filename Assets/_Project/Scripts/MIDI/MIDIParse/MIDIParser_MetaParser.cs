﻿using System;
using System.Text;

namespace UnityMidiKit
{
    public static partial class MIDIParser
    {
        private static bool IsMetaEvent(byte command)
        {
            return (command & 0xff) == 0xff;
        }

        private static MIDIMetaEvent ParseMetaEvent(int atTick, byte[] byteBuffer, ref int byteBufferIndex)
        {
            if (IsMetaEvent(byteBuffer[byteBufferIndex]))
            {
                byteBufferIndex++;
                MIDIMetaEventType eventType = GetMetaEventType(byteBuffer[byteBufferIndex]);
                
                byteBufferIndex++;
                int length = GetVariableLength(byteBuffer, ref byteBufferIndex);

                byte[] data = new byte[length];
                Array.Copy(byteBuffer, byteBufferIndex, data, 0, length);
                byteBufferIndex += length;

                return eventType.GetClassInstance(atTick, data);
            }
            else
            {
                return null;
            }
        }

        private static MIDIMetaEventType GetMetaEventType(byte command)
        {
            MIDIMetaEventType metaEvent = MIDIMetaEventType.Unknown;
            if (Enum.IsDefined(typeof(MIDIMetaEventType), (int)command))
            {
                metaEvent = (MIDIMetaEventType)command;
            }
            return metaEvent;
        }
    }
}
