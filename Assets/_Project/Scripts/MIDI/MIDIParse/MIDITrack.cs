﻿using System.Collections.Generic;

namespace UnityMidiKit
{
    public class MIDITrack
    {
        public int Ticks { get; internal set; }

        private Dictionary<int, List<MIDIMessage>> m_messages;
        private Dictionary<int, List<MIDIMetaEvent>> m_metaEvents;

        public MIDITrack()
        {
            m_messages = new Dictionary<int, List<MIDIMessage>>();
            m_metaEvents = new Dictionary<int, List<MIDIMetaEvent>>();
        }

        public List<MIDIMessage> GetAllMIDIMessages()
        {
            List<MIDIMessage> result = new List<MIDIMessage>();
            foreach (List<MIDIMessage> messageList in m_messages.Values)
            {
                result.AddRange(messageList);
            }
            return result;
        }

        public List<MIDIMessage> GetMIDIMessagesForTick(int tick)
        {
            List<MIDIMessage> result = null;
            m_messages.TryGetValue(tick, out result);
            return result;
        }

        public List<MIDIMetaEvent> GetMIDIMetaEventsForTick(int tick)
        {
            List<MIDIMetaEvent> result = null;
            m_metaEvents.TryGetValue(tick, out result);
            return result;
        }

        internal void AppendMessages(List<MIDIMessage> messages)
        {
            foreach (MIDIMessage msg in messages)
            {
                List<MIDIMessage> existingMessages = null;
                if (!m_messages.TryGetValue(msg.Tick, out existingMessages))
                {
                    existingMessages = new List<MIDIMessage>();
                    m_messages.Add(msg.Tick, existingMessages);
                }
                existingMessages.Add(msg);
            }
        }

        internal void AppendMetaEvent(MIDIMetaEvent me)
        {
            if (me != null)
            {
                List<MIDIMetaEvent> existingMessages = null;
                if (!m_metaEvents.TryGetValue(me.Tick, out existingMessages))
                {
                    existingMessages = new List<MIDIMetaEvent>();
                    m_metaEvents.Add(me.Tick, existingMessages);
                }
                existingMessages.Add(me);
            }
        }

        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (int key in m_messages.Keys)
            {
                sb.Append(key.ToString() + ", ");
            }
            return sb.ToString();
        }
    }
}