﻿using System;
using System.Collections.Generic;

namespace UnityMidiKit
{
    public static partial class MIDIParser
    {
        public static List<MIDIMessage> ParseMessages(int atTick, byte commandByte, byte[] data)
        {
            int bytesToRead = BytesToReadForStatus(commandByte);
            byte[] messageData = new byte[bytesToRead];
            
            int numberOfMessages = data.Length / bytesToRead;
            List<MIDIMessage> messages = new List<MIDIMessage>();
            for (int messageIndex = 0; messageIndex < numberOfMessages; messageIndex++)
            {
                Array.Copy(data, 0, messageData, 0, bytesToRead);
                MIDIMessage mm = ParseMessage(atTick, commandByte, messageData);
                if (mm != null)
                {
                    messages.Add(mm);
                }
            }

            return messages;
        }

        public static MIDIMessage ParseMessage(int atTick, byte commandByte, byte[] dataBytes)
        {
            MIDIEventType eventType = GetStatus(commandByte);
            MIDIMessage msg = eventType.GetClassInstance(atTick, commandByte, dataBytes);
            if (msg == null)
            {
                MIDI.Log("[MIDIParser] Unhandled MIDIEventType: " + eventType);
            }
            return msg;
        }

        private static MIDIEventType GetStatus(byte command)
        {
            MIDIEventType status = MIDIEventType.Unknown;

            int statusInteger = command & 0xf0;
            if (statusInteger == 0xf0)
            {
                statusInteger = command & 0xff;
            }

            if (Enum.IsDefined(typeof(MIDIEventType), statusInteger))
            {
                status = (MIDIEventType)statusInteger;
            }

            return status;
        }

        private static int BytesToReadForStatus(byte command)
        {
            MIDIEventType et = GetStatus(command);
            switch (et)
            {
                case MIDIEventType.NoteOn:
                case MIDIEventType.NoteOff:
                case MIDIEventType.ControlChangeOrChannelMode:
                case MIDIEventType.PitchBendChange:
                case MIDIEventType.PolyphonicKeyPressure:
                    {
                        return 2;
                    }

                case MIDIEventType.ProgramChange:
                case MIDIEventType.ChannelPressure:
                    {
                        return 1;
                    }

                default:
                case MIDIEventType.Unknown:
                    {
                        return 0;
                    }
            }
        }
    }
}
