﻿using System.Text;

namespace UnityMidiKit
{
    public static partial class MIDIParser
    {
        private static bool IsSystemExclusive(byte b)
        {
            return b == 0xf0 || b == 0xf7;
        }

        private static string ParseSystemExclusive(byte[] byteBuffer, ref int byteBufferIndex)
        {
            if (IsSystemExclusive(byteBuffer[byteBufferIndex]))
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("- SystemExclusive: ");
                byteBufferIndex++;
                int length = GetVariableLength(byteBuffer, ref byteBufferIndex);

                for (int sysExIndex = byteBufferIndex; sysExIndex < byteBufferIndex + length; sysExIndex++)
                {
                    sb.Append(byteBuffer[sysExIndex].ToString("x2"));
                    if (sysExIndex < (byteBufferIndex + length - 1))
                    {
                        sb.Append(" ");
                    }
                }

                sb.AppendLine();
                byteBufferIndex += length;

                return sb.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}
