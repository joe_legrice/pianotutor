﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace UnityMidiKit
{
    public class MIDITrackStepper
    {
        private int m_firstTick;
        private MIDITrack m_track;
        private List<MIDINote> m_notes = new List<MIDINote>();

        public int NumberOfBars { get; private set; }
        public List<MIDINote> AllNotes { get { return m_notes; } }

        public MIDITrackStepper(MIDITrack track, int ticksPerQuarter)
        {
            m_track = track;

            m_firstTick = int.MaxValue;
            foreach (MIDIMessage msg in m_track.GetAllMIDIMessages())
            {
                if (msg.GetEventType() == MIDIEventType.NoteOn)
                {
                    m_firstTick = Mathf.Min(msg.Tick, m_firstTick);
                }
            }

            List<MIDINoteOnMessage> cachedStartMessages = new List<MIDINoteOnMessage>();
            for (int tick = 0; tick < m_track.Ticks; tick++)
            {
                List<MIDIMessage> messages = m_track.GetMIDIMessagesForTick(tick);
                if (messages != null)
                {
                    foreach (MIDIMessage msg in messages)
                    {
                        if (msg.GetEventType() == MIDIEventType.NoteOn)
                        {
                            cachedStartMessages.Add((MIDINoteOnMessage)msg);
                        }
                        else if (msg.GetEventType() == MIDIEventType.NoteOff)
                        {
                            MIDINoteOffMessage noteOffMsg = (MIDINoteOffMessage)msg;
                            MIDINoteOnMessage startMessage = cachedStartMessages.Find(m => {
                                return m.Pitch == noteOffMsg.Pitch;
                            });

                            if (startMessage != null)
                            {
                                MIDINote mn = new MIDINote(startMessage.Pitch,
                                    startMessage.Velocity,
                                    startMessage.Tick - m_firstTick,
                                    tick - m_firstTick
                                );
                                m_notes.Add(mn);
                                cachedStartMessages.Remove(startMessage);
                            }
                        }
                    }
                }
            }

            m_notes.Sort((m1, m2) => { return m1.StartTick - m2.StartTick; });

            // TODO: Support different time signatures
            NumberOfBars = Mathf.CeilToInt((float)(m_track.Ticks - m_firstTick) / (4 * ticksPerQuarter));
        }
        
        public List<MIDINote> GetMessagesForPosition(int tick)
        {
            return m_notes.Where(mn => {
                return mn.StartTick <= tick && mn.ReleaseTick >= tick;
            }).ToList();
        }

        public List<MIDINote> GetMessagesForRange(int startTick, int endTick)
        {
            return m_notes.Where(mn => {
                return mn.StartTick >= startTick && mn.ReleaseTick <= endTick;
            }).ToList();
        }
    }

    public class MIDINote
    {
        public Pitch Pitch { get; private set; }
        public float Velocity { get; private set; }
        public int StartTick { get; private set; }
        public int ReleaseTick { get; private set; }

        public MIDINote(Pitch p, float v, int startTick, int endTick)
        {
            Pitch = p;
            Velocity = v;
            StartTick = startTick;
            ReleaseTick = endTick;
        }
    }
}
