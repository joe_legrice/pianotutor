﻿using UnityEngine;
using System.Collections.Generic;

namespace UnityMidiKit
{
    public class MIDISequenceStepper
    {
        public MIDISequence Sequence { get; private set; }

        private int m_currentIndex;
        private int m_currentTick;
        private int m_ticksPer32Note;
        private List<MIDINote> m_currentNotes;
        private List<List<MIDINote>> m_groupedNotes;
        private List<MIDITrackStepper> m_trackSteppers;

        // TODO: Support Different Time Signatures
        public List<MIDINote> AllNotes { get; private set; }
        public int CurrentBar { get { return m_currentTick / TicksPerBar; } }
        public int TicksPerBar { get { return 4 * Sequence.TicksPerQuarterNote; } }
        public int NumberOfBars { get; private set; }
        public int NumberOfTicks { get { return NumberOfBars * TicksPerBar; } }

        public MIDISequenceStepper(MIDISequence sequence)
        {
            Sequence = sequence;
            m_ticksPer32Note = Sequence.TicksPerQuarterNote / 8;

            m_currentTick = -1;
            m_currentIndex = 0;
            m_currentNotes = new List<MIDINote>();
            m_trackSteppers = new List<MIDITrackStepper>();
            for (int trackIndex = 0; trackIndex < Sequence.NumberOfTracks; trackIndex++)
            {
                m_trackSteppers.Add(new MIDITrackStepper(Sequence.GetTrack(trackIndex), Sequence.TicksPerQuarterNote));
            }

            // TODO: Support different MIDISequence Modes
            AllNotes = new List<MIDINote>();
            foreach (MIDITrackStepper ts in m_trackSteppers)
            {
                NumberOfBars = Mathf.Max(NumberOfBars, ts.NumberOfBars);
                AllNotes.AddRange(ts.AllNotes);
            }
            AllNotes.Sort((m1, m2) => { return m1.StartTick - m2.StartTick; });

            int insertionIndex = 0;
            m_groupedNotes = new List<List<MIDINote>>();
            for (int innerIndex = 0; innerIndex < AllNotes.Count; innerIndex++)
            {
                List<MIDINote> notes = new List<MIDINote>();

                MIDINote mn1 = AllNotes[innerIndex];
                int maxTick = mn1.ReleaseTick;
                for (int outerIndex = innerIndex + 1; outerIndex < AllNotes.Count; outerIndex++)
                {
                    MIDINote mn2 = AllNotes[outerIndex];
                    if (mn2.StartTick < maxTick)
                    {
                        notes.Add(mn2);
                        maxTick = Mathf.Max(maxTick, mn2.ReleaseTick);
                    }

                }

                if (notes.Count > 0)
                {
                    notes.Sort((m1, m2) => { return m1.StartTick - m2.StartTick; });
                    m_groupedNotes.Add(notes);
                    insertionIndex++;
                }
                innerIndex += Mathf.Max(notes.Count - 1, 0);
            }
        }

        public void SetTrackPosition(int tick)
        {
            tick = Mathf.Clamp(tick, 0, NumberOfTicks);

            int currentMin;
            int currentMax;
            GetCurrentMinMaxTicks(out currentMin, out currentMax);

            while (m_currentTick != tick)
            {
                if (tick > m_currentTick)
                {
                    m_currentTick++;
                }
                else
                {
                    m_currentTick--;
                }

                int lastMin = -1;
                int lastMax = -1;
                while ((m_currentTick >= currentMax || m_currentTick <= currentMin) && (
                    lastMax != currentMax && lastMin != currentMin))
                {
                    m_currentIndex += tick > m_currentTick ? 1 : -1;
                    m_currentIndex = Mathf.Clamp(m_currentIndex, 0, m_groupedNotes.Count - 1);

                    lastMin = currentMin;
                    lastMax = currentMax;
                    GetCurrentMinMaxTicks(out currentMin, out currentMax);
                }

                m_currentNotes.Clear();
                foreach (MIDINote mn in m_groupedNotes[m_currentIndex])
                {
                    if (mn.StartTick <= m_currentTick && mn.ReleaseTick >= m_currentTick)
                    {
                        m_currentNotes.Add(mn);
                    }
                }
            }
        }

        public List<MIDINote> GetMessagesForCurrentPosition()
        {
            return m_currentNotes;
        }

        private void GetCurrentMinMaxTicks(out int min, out int max)
        {
            min = int.MaxValue;
            max = int.MinValue;
            foreach (MIDINote mn in m_groupedNotes[m_currentIndex])
            {
                min = Mathf.Min(min, mn.StartTick);
                max = Mathf.Max(max, mn.ReleaseTick);
            }
        }
    }
}
