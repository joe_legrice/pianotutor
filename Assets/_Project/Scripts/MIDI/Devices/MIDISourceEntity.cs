﻿using System;

namespace UnityMidiKit
{
    public partial class MIDISourceEntity
    {
        public int EntityID { get; private set; }
        public bool IsOpen { get; private set; }
        public bool IsReceiving { get; private set; }

        public event Action<MIDINoteOnMessage> OnNotePressed;
        public event Action<MIDINoteOffMessage> OnNoteReleased;
    }
}
