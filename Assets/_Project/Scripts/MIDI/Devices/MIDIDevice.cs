﻿using System;
using System.Collections.Generic;

namespace UnityMidiKit
{
    public partial class MIDIDevice
    {
        public int DeviceID { get; private set; }
        public string Name { get; private set; }
        public string Manufacturer { get; private set; }
        public List<MIDIDestinationEntity> DeviceInputs { get; private set; }
        public List<MIDISourceEntity> DeviceOutputs { get; private set; }
        
        public event Action<MIDIMessage> OnNotePressed;
        public event Action<MIDIMessage> OnNoteReleased;
        
        public override string ToString()
        {
            return "[MIDIDevice: DeviceID=" + DeviceID + ", Name=" + Name + ", Manufacturer=" + Manufacturer + "]";
        }

        public MIDISourceEntity GetSourceEntityWithID(int entityId)
        {
            return DeviceOutputs.Find(e => { return e.EntityID == entityId; });
        }

        public MIDIDestinationEntity GetDestinationEntityWithID(int entityId)
        {
            return DeviceInputs.Find(e => { return e.EntityID == entityId; });
        }

        public bool HasOutputOpen()
        {
            foreach (MIDISourceEntity se in DeviceOutputs)
            {
                if (se.IsOpen)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
