﻿#if UNITY_STANDALONE_OSX
using Newtonsoft.Json.Linq;

namespace UnityMidiKit
{
    public partial class MIDISourceEntity
    {
        public static MIDISourceEntity ParseFromJToken(JToken jt)
        {
            return null;
        }
        
        public bool Open()
        {
            if (!IsOpen)
            {
                return IsOpen;
            }
            return false;
        }
        
        public bool Close()
        {
            if (IsOpen)
            {
                return !IsOpen;
            }
            return false;
        }
        
        public bool StartReceiving()
        {
            if (!IsReceiving)
            {
                return IsReceiving;
            }
            return false;
        }
        
        public bool StopReceiving()
        {
            if (IsReceiving)
            {
                return !IsReceiving;
            }
            return false;
        }
    }
}
#endif
