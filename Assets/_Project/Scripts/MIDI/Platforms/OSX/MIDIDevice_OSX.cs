﻿#if UNITY_STANDALONE_OSX
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace UnityMidiKit
{
    public partial class MIDIDevice
	{
		public static MIDIDevice ParseFromJToken(JToken jt)
        {
            if (jt.Type == JTokenType.Object)
            {
                JObject jo = (JObject)jt;
                if (jo.TryGetValue("error", out jt))
                {
                    UnityEngine.Debug.LogWarning("[MIDIDevice] Could not Parse JSON: "+jt.ToString());
                    return null;
                }
                else
                {
                    MIDIDevice device = new MIDIDevice();
                        
                    if (jo.TryGetValue("uniqueID", out jt))
                    {
                        device.DeviceID = jt.ToObject<int>();
                    }
                    
                    if (jo.TryGetValue("name", out jt))
                    {
                        device.Name = jt.ToObject<string>();
                    }
                    
                    if (jo.TryGetValue("manufacturer", out jt))
                    {
                        device.Manufacturer = jt.ToObject<string>();
                    }
                    
                    device.DeviceInputs = new List<MIDIDestinationEntity>();
                    device.DeviceOutputs = new List<MIDISourceEntity>();
                    if (jo.TryGetValue("entities", out jt))
                    {
                        JArray entities = (JArray)jt;
                        for (int entIndex=0; entIndex < entities.Count; entIndex++)
                        {
                            JObject ent = (JObject)entities[entIndex];
                            if (ent.TryGetValue("sources", out jt))
                            {
                                JArray sourceArr = (JArray)jt;
                                for (int sourceIndex=0; sourceIndex < sourceArr.Count; sourceIndex++)
                                {
                                    MIDISourceEntity se = MIDISourceEntity.ParseFromJToken(sourceArr[sourceIndex]);
                                    if (se != null)
                                    {
                                        device.DeviceOutputs.Add(se);
                                    }
                                }
                            }
                            if (ent.TryGetValue("destinations", out jt))
                            {
                                JArray destArr = (JArray)jt;
                                for (int destIndex=0; destIndex < destArr.Count; destIndex++)
                                {
                                    device.DeviceInputs.Add(new MIDIDestinationEntity(destArr[destIndex]));
                                }
                            }
                        }
                        return device;
                    }
                }
            }
            else if (jt.Type == JTokenType.Array)
            {
                JArray ja = (JArray)jt;
                UnityEngine.Debug.Log("[MIDIDevice] MIDIDevice Token: " + ja.ToString());
                return null;
            }
            return null;
        }
	}
}
#endif
