﻿#if UNITY_STANDALONE_OSX
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Newtonsoft.Json.Linq;

namespace UnityMidiKit
{
    public static partial class MIDI
    {
        private delegate void UnityMIDIMessageHook(Byte[] data);

        [DllImport("UnityMIDI")]
        private static extern void InitialiseUnityMIDI(UnityMIDIMessageHook destinationHook);

        [DllImport("UnityMIDI")]
        private static extern void SendMIDI(Byte[] midiData);

        [DllImport("UnityMIDI")]
        private static extern IntPtr GetDevicePropertiesJSON();

        public static void RefreshDeviceList()
        {
            if (Initialised)
            {
                MIDI.Log("[MIDI] Refreshing Device List..");

                s_allDevices = new List<MIDIDevice>();
                IntPtr p = GetDevicePropertiesJSON();
                string jsonString = Marshal.PtrToStringAnsi(p);

                JArray deviceArray = JArray.Parse(jsonString.ToString());
                foreach (JToken jt in deviceArray)
                {
                    MIDIDevice device = MIDIDevice.ParseFromJToken(jt);
                    if (device != null)
                    {
                        s_allDevices.Add(device);
                        MIDI.Log("[MIDI] Found MIDI Device: " + device);
                    }
                }
                MIDI.Log("[MIDI] Refreshed Device List with [" + s_allDevices.Count + "] devices");
            }
        }

        public static void Initialise()
        {
            if (!Initialised)
            {
                MIDI.Log("[MIDI] Initializing MIDI...");

                Initialised = true;
                InitialiseUnityMIDI((data) => {
                    MIDI.Log("Got some data");
                });

                MIDI.Log("[MIDI] Initializing MIDI... Done");

                RefreshDeviceList();
            }
        }

        public static void SendMIDI()
        {
            if (Initialised)
            {
                SendMIDI(null);
            }
        }
    }
}
#endif
