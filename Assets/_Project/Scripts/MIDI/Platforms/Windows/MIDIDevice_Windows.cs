﻿#if UNITY_STANDALONE_WIN
using System.Collections.Generic;

namespace UnityMidiKit
{
	public partial class MIDIDevice
	{
		public MIDIDevice(string name, string manufacturer, List<Win32API.MIDIINCAPS> inputs, List<Win32API.MIDIOUTCAPS> outputs)
        {
            Name = name;
            Manufacturer = manufacturer;

            DeviceInputs = new List<MIDIDestinationEntity>();
            if (outputs != null)
            {
                for (int deviceIndex = 0; deviceIndex < outputs.Count; deviceIndex++)
                {
                    Win32API.MIDIOUTCAPS outputCapabilities = outputs[deviceIndex];
                    DeviceInputs.Add(new MIDIDestinationEntity(deviceIndex, outputCapabilities));
                }
            }

            DeviceOutputs = new List<MIDISourceEntity>();
            if (inputs != null)
            {
                for (int deviceIndex = 0; deviceIndex < inputs.Count; deviceIndex++)
                {
                    Win32API.MIDIINCAPS inputCapabilities = inputs[deviceIndex];
                    DeviceOutputs.Add(new MIDISourceEntity(deviceIndex, inputCapabilities));
                }
            }
        }
    }
}
#endif
