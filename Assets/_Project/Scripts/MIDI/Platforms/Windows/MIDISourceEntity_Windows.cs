﻿#if UNITY_STANDALONE_WIN
using System;
using UnityEngine;

namespace UnityMidiKit
{
    public partial class MIDISourceEntity
    {
        private Win32API.HMIDIIN m_handle;

        public MIDISourceEntity(int deviceIndex, Win32API.MIDIINCAPS inputCapabilities)
        {
            EntityID = deviceIndex;

            MIDI.Log("[MIDISourceEntity] szPname: " + inputCapabilities.szPname);
            MIDI.Log("[MIDISourceEntity] vDriverVersion: " + inputCapabilities.vDriverVersion);
            MIDI.Log("[MIDISourceEntity] wMid: " + inputCapabilities.wMid);
            MIDI.Log("[MIDISourceEntity] wPid: " + inputCapabilities.wPid);
            MIDI.Log("[MIDISourceEntity] dwSupport: " + inputCapabilities.dwSupport);
        }

        public bool Open()
        {
            if (!IsOpen)
            {
                IsOpen = HandleResult(Win32API.midiInOpen(out m_handle, (UIntPtr)EntityID, HandleInput, (UIntPtr)EntityID));
                return IsOpen;
            }
            return true;
        }

        public bool Close()
        {
            if (!StopReceiving())
            {
                return false;
            }

            if (IsOpen)
            {
                IsOpen = !HandleResult(Win32API.midiInClose(m_handle));
                return !IsOpen;
            }
            return true;
        }

        public bool StartReceiving()
        {
            if (!Open())
            {
                return false;
            }

            if (!IsReceiving)
            {
                IsReceiving = HandleResult(Win32API.midiInStart(m_handle));
                return IsReceiving;
            }
            return true;
        }

        public bool StopReceiving()
        {
            if (IsReceiving)
            {
                IsReceiving = !HandleResult(Win32API.midiInStop(m_handle));
                return !IsReceiving;
            }
            return true;
        }

        private bool HandleResult(Win32API.MMRESULT result)
        {
            if (result == Win32API.MMRESULT.MMSYSERR_NOERROR)
            {
                return true;
            }
            else
            {
                MIDI.LogError("[MIDISourceEntity] Error: " + result);
                return false;
            }
        }

        private static void HandleInput(Win32API.HMIDIIN hMidiIn, Win32API.MidiInMessage wMsg, UIntPtr dwInstance, UIntPtr dwParam1, UIntPtr dwParam2)
        {
            //MIDI.Log("[MIDISourceEntity] HandleInput(" + hMidiIn + ", " + wMsg + ", " + dwInstance + ", " + dwParam1 + ", " + dwParam2 + ")");

            if (wMsg == Win32API.MidiInMessage.MIM_DATA)
            {
                long value = unchecked((long)(ulong)dwParam1);
                byte[] bytes = BitConverter.GetBytes(value);
                byte command = bytes[0];
                byte[] data = new byte[2] { bytes[1], bytes[2] };
                
                MIDIMessage msg = MIDIParser.ParseMessage(0, command, data);

                if (msg != null)
                {
                    MIDI.Log("[MIDISourceEntity] msg = " + msg);

                    MIDISourceEntity source = null;
                    foreach (MIDIDevice d in MIDI.AllDevices)
                    {
                        source = d.GetSourceEntityWithID((int)dwInstance);
                        if (source != null)
                        {
                            break;
                        }
                    }

                    if (source != null)
                    {
                        UnityMainThreadDispatch.Enqueue(() => {
                            switch (msg.GetEventType())
                            {
                                case MIDIEventType.NoteOn:
                                    {
                                        if (source.OnNotePressed != null)
                                        {
                                            source.OnNotePressed((MIDINoteOnMessage)msg);
                                        }
                                        break;
                                    }

                                case MIDIEventType.NoteOff:
                                    {
                                        if (source.OnNoteReleased != null)
                                        {
                                            source.OnNoteReleased((MIDINoteOffMessage)msg);
                                        }
                                        break;
                                    }

                                default:
                                    {
                                        break;
                                    }
                            }
                        });
                    }
                }
            }
        }
    }
}
#endif