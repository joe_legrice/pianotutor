﻿using System;
using System.Runtime.InteropServices;

using System.Text;

namespace UnityMidiKit
{
#if UNITY_STANDALONE_WIN
    public static class Win32API
    {
        private const UInt32 MAXPNAMELEN = 32;

        public enum MMRESULT : uint
        {
            // General return codes.
            MMSYSERR_NOERROR = 0,
            MMSYSERR_ERROR = 1,
            MMSYSERR_BADDEVICEID = 2,
            MMSYSERR_NOTENABLED = 3,
            MMSYSERR_ALLOCATED = 4,
            MMSYSERR_INVALHANDLE = 5,
            MMSYSERR_NODRIVER = 6,
            MMSYSERR_NOMEM = 7,
            MMSYSERR_NOTSUPPORTED = 8,
            MMSYSERR_BADERRNUM = 9,
            MMSYSERR_INVALFLAG = 10,
            MMSYSERR_INVALPARAM = 11,
            MMSYSERR_HANDLEBUSY = 12,
            MMSYSERR_INVALIDALIAS = 13,
            MMSYSERR_BADDB = 14,
            MMSYSERR_KEYNOTFOUND = 15,
            MMSYSERR_READERROR = 16,
            MMSYSERR_WRITEERROR = 17,
            MMSYSERR_DELETEERROR = 18,
            MMSYSERR_VALNOTFOUND = 19,
            MMSYSERR_NODRIVERCB = 20,
            MMSYSERR_MOREDATA = 21,

            // MIDI-specific return codes.
            MIDIERR_UNPREPARED = 64,
            MIDIERR_STILLPLAYING = 65,
            MIDIERR_NOMAP = 66,
            MIDIERR_NOTREADY = 67,
            MIDIERR_NODEVICE = 68,
            MIDIERR_INVALIDSETUP = 69,
            MIDIERR_BADOPENMODE = 70,
            MIDIERR_DONT_CONTINUE = 71
        }

        public enum MidiInMessage : uint
        {
            MIM_OPEN = 0x3C1,
            MIM_CLOSE = 0x3C2,
            MIM_DATA = 0x3C3,
            MIM_LONGDATA = 0x3C4,
            MIM_ERROR = 0x3C5,
            MIM_LONGERROR = 0x3C6,
            MIM_MOREDATA = 0x3CC
        }

        public enum MidiOpenFlags : uint
        {
            CALLBACK_TYPEMASK = 0x70000,
            CALLBACK_NULL = 0x00000,
            CALLBACK_WINDOW = 0x10000,
            CALLBACK_TASK = 0x20000,
            CALLBACK_FUNCTION = 0x30000,
            CALLBACK_THREAD = CALLBACK_TASK,
            CALLBACK_EVENT = 0x50000,
            MIDI_IO_STATUS = 0x00020
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct HMIDIIN
        {
            public Int32 handle;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MIDIINCAPS
        {
            public UInt16 wMid;
            public UInt16 wPid;
            public UInt32 vDriverVersion;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = (int)MAXPNAMELEN)]
            public string szPname;
            public UInt32 dwSupport;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MIDIOUTCAPS
        {
            public UInt16 wMid;
            public UInt16 wPid;
            public UInt32 vDriverVersion;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = (int)MAXPNAMELEN)]
            public string szPname;
            public UInt16 wTechnology;
            public UInt16 wVoices;
            public UInt16 wNotes;
            public UInt16 wChannelMask;
            public UInt32 dwSupport;
        }

        public delegate void MidiInProc(HMIDIIN hMidiIn, MidiInMessage wMsg, UIntPtr dwInstance, UIntPtr dwParam1, UIntPtr dwParam2);

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern UInt32 midiInGetNumDevs();

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern MMRESULT midiInClose(HMIDIIN hMidiIn);

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern MMRESULT midiInStart(HMIDIIN hMidiIn);

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern MMRESULT midiInStop(HMIDIIN hMidiIn);

        [DllImport("winmm.dll", SetLastError = true)]
        public static extern MMRESULT midiInReset(HMIDIIN hMidiIn);

        [DllImport("winmm.dll", SetLastError = true)]
        private static extern MMRESULT midiInOpen(out HMIDIIN lphMidiIn, UIntPtr uDeviceID, MidiInProc dwCallback, UIntPtr dwCallbackInstance, MidiOpenFlags dwFlags);
        public static MMRESULT midiInOpen(out HMIDIIN lphMidiIn, UIntPtr uDeviceID, MidiInProc dwCallback, UIntPtr dwCallbackInstance)
        {
            return midiInOpen(out lphMidiIn, uDeviceID, dwCallback, dwCallbackInstance,
                dwCallback == null ? MidiOpenFlags.CALLBACK_NULL : MidiOpenFlags.CALLBACK_FUNCTION);
        }

        public static MMRESULT midiInGetErrorText(MMRESULT mmrError, StringBuilder lpText)
        {
            return midiInGetErrorText(mmrError, lpText, (UInt32)lpText.Capacity);
        }
        [DllImport("winmm.dll", SetLastError = true)]
        private static extern MMRESULT midiInGetErrorText(MMRESULT wError, StringBuilder lpText, UInt32 cchText);

        [DllImport("winmm.dll", SetLastError = true)]
        private static extern MMRESULT midiInGetDevCaps(UIntPtr uDeviceID, out MIDIINCAPS lpMidiInCaps, UInt32 cbMidiInCaps);
        public static MMRESULT midiInGetDevCaps(UIntPtr uDeviceID, out MIDIINCAPS caps)
        {
            return midiInGetDevCaps(uDeviceID, out caps, (UInt32)Marshal.SizeOf(typeof(MIDIINCAPS)));
        }
    }
#endif
}
