﻿#if UNITY_STANDALONE_WIN
using System;
using System.Linq;
using System.Collections.Generic;

namespace UnityMidiKit
{
    public static partial class MIDI
    {
        public static void RefreshDeviceList()
        {
            if (Initialised)
            {
                MIDI.Log("[MIDI] Refreshing Device List..");
                s_allDevices = new List<MIDIDevice>();

                int numInDevices = (int)Win32API.midiInGetNumDevs();
                List<Win32API.MIDIINCAPS> allInDevices = new List<Win32API.MIDIINCAPS>();
                for (int deviceIndex = 0; deviceIndex < numInDevices; deviceIndex++)
                {
                    Win32API.MIDIINCAPS inDeviceCapabilities = new Win32API.MIDIINCAPS();
                    Win32API.midiInGetDevCaps((UIntPtr)deviceIndex, out inDeviceCapabilities);
                    allInDevices.Add(inDeviceCapabilities);
                }
                IDictionary<string, List<Win32API.MIDIINCAPS>> groupedInDevices = allInDevices
                    .GroupBy(inCaps => { return inCaps.szPname; })
                    .ToDictionary(
                        grouping => { return grouping.Key; },
                        grouping => { return grouping.ToList(); }
                    );

                List<Win32API.MIDIOUTCAPS> allOutDevices = new List<Win32API.MIDIOUTCAPS>();
                // TODO: Populate allOutDevices
                IDictionary<string, List<Win32API.MIDIOUTCAPS>> groupedOutDevices = allOutDevices
                    .GroupBy(outCaps => { return outCaps.szPname; })
                    .ToDictionary(
                        grouping => { return grouping.Key; },
                        grouping => { return grouping.ToList(); }
                    );

                IEnumerable<string> allKeys = groupedInDevices
                    .Select(inDev => { return inDev.Key; })
                    .Concat(groupedOutDevices.Select(outDev => { return outDev.Key; }))
                    .Distinct();

                foreach (string productName in allKeys)
                {
                    List<Win32API.MIDIINCAPS> sources = null;
                    groupedInDevices.TryGetValue(productName, out sources);

                    List<Win32API.MIDIOUTCAPS> destinations = null;
                    groupedOutDevices.TryGetValue(productName, out destinations);

                    MIDIDevice inputDevice = new MIDIDevice(productName, null, sources, destinations);
                    s_allDevices.Add(inputDevice);
                    MIDI.Log("[MIDI] Found MIDI Device: " + inputDevice);
                }
                MIDI.Log("[MIDI] Refreshed Device List with [" + s_allDevices.Count + "] devices");
            }
        }
        
        public static void Initialise()
        {
            MIDI.Log("[MIDI] Initializing MIDI...");
            Initialised = true;
            UnityMainThreadDispatch.Begin();
            MIDI.Log("[MIDI] Initializing MIDI... Done");
            RefreshDeviceList();
        }

        public static void SendMIDI()
        {
        }
    }
}
#endif
