﻿using UnityEngine;
using System.Collections.Generic;

namespace UnityMidiKit
{
    public static partial class MIDI
    {
        public static bool s_loggingEnabled = true;

        private static List<MIDIDevice> s_allDevices;
        public static List<MIDIDevice> AllDevices
        {
            get
            {
                if (s_allDevices == null)
                {
                    RefreshDeviceList();
                }
                return s_allDevices;
            }
        }
        
        public static bool Initialised { get; private set; }

        static internal void Log(object message)
        {
            if (s_loggingEnabled)
            {
                Debug.Log(message.ToString());
            }
        }

        static internal void LogError(object message)
        {
            Debug.LogError(message.ToString());
        }

        public static MIDISourceEntity FindSourceEntityWithID(int id)
        {
            if (AllDevices != null)
            {
                foreach (MIDIDevice d in AllDevices)
                {
                    MIDISourceEntity e = d.GetSourceEntityWithID(id);
                    if (e != null)
                    {
                        return e;
                    }
                }
            }
            return null;
        }

        public static MIDIDestinationEntity FindDestinationEntityWithID(int id)
        {
            if (AllDevices != null)
            {
                foreach (MIDIDevice d in AllDevices)
                {
                    MIDIDestinationEntity e = d.GetDestinationEntityWithID(id);
                    if (e != null)
                    {
                        return e;
                    }
                }
            }
            return null;
        }
    }
}
