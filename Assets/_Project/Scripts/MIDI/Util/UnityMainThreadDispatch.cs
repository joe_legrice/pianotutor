﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class UnityMainThreadDispatch : MonoBehaviour
{
    private static UnityMainThreadDispatch s_instance;

    public static void Begin()
    {
        GameObject go = new GameObject();
        s_instance = go.AddComponent<UnityMainThreadDispatch>();
        go.name = "UnityMainThreadDispatch";
    }

    public static void Enqueue(Action a)
    {
        if (s_instance != null)
        {
            lock (s_instance.m_lock)
            {
                s_instance.m_actions.Add(a);
            }
        }
    }

    private readonly object m_lock = new object();
    private List<Action> m_actions = new List<Action>();
    
    private void Update()
    {
        lock (m_lock)
        {
            if (m_actions.Count > 0)
            {
                foreach (Action a in m_actions)
                {
                    a();
                }
                m_actions.Clear();
            }
        }
    }
}
