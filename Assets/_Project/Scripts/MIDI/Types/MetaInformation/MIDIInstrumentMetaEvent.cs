﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDIInstrumentMetaEvent : MIDIMetaEvent
    {
        public string InstrumentName { get; private set; }

        public MIDIInstrumentMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            InstrumentName = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.Instrument;
        }

        public override string ToString()
        {
            return "[MIDIInstrumentMetaEvent InstrumentName=" + InstrumentName + "]";
        }
    }
}
