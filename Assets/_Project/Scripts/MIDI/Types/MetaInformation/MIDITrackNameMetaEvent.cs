﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDITrackNameMetaEvent : MIDIMetaEvent
    {
        public string Name { get; private set; }

        public MIDITrackNameMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            Name = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.TrackName;
        }

        public override string ToString()
        {
            return "[MIDITrackNameMetaEvent Name=" + Name + "]";
        }
    }
}
