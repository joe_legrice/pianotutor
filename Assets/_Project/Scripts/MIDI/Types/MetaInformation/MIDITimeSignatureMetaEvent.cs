﻿using UnityEngine;

namespace UnityMidiKit
{
    public class MIDITimeSignatureMetaEvent : MIDIMetaEvent
    {
        public TimeSignature Signature { get; private set; }
        public int MIDIClocksPerMetronomeTick { get; private set; }
        public int NumberOf32NotesPer24MIDIClocks { get; private set; }

        public MIDITimeSignatureMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            Signature = new TimeSignature(data[0], (int)Mathf.Pow(2, data[1]));
            MIDIClocksPerMetronomeTick = data[2];
            NumberOf32NotesPer24MIDIClocks = data[3];
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.TimeSignature;
        }
        
        public override string ToString()
        {
            return "[MIDITimeSignatureMetaEvent Signature=" + Signature + ", MIDIClocksPerMetronomeTick=" + MIDIClocksPerMetronomeTick + ", NumberOf32NotesPer24MIDIClocks=" + NumberOf32NotesPer24MIDIClocks + "]";
        }
    }
}
