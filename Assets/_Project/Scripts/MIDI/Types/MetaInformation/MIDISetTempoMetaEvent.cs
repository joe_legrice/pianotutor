﻿using System;

namespace UnityMidiKit
{
    public class MIDISetTempoMetaEvent : MIDIMetaEvent
    {
        public int MicrosecondsPerQuaterNote { get; private set; }

        public MIDISetTempoMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            byte[] fourByteArray = new byte[] { new byte(), new byte(), new byte(), new byte() };
            Array.Copy(data, fourByteArray, data.Length);
            MicrosecondsPerQuaterNote = BitConverter.ToInt32(fourByteArray, 0);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.SetTempo;
        }

        public override string ToString()
        {
            return "[MIDISetTempoMetaEvent MicrosecondsPerQuaterNote=" + MicrosecondsPerQuaterNote+"]";
        }
    }
}
