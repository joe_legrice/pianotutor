﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDILyricMetaEvent : MIDIMetaEvent
    {
        public string Text { get; private set; }

        public MIDILyricMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            Text = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.Lyrics;
        }

        public override string ToString()
        {
            return "[MIDILyricMetaEvent Text=" + Text + "]";
        }
    }
}
