﻿namespace UnityMidiKit
{
    public abstract class MIDIMetaEvent
    {
        public int Tick { get; private set; }
        public abstract MIDIMetaEventType GetEventType();

        public MIDIMetaEvent(int atTick, byte[] data)
        {
            Tick = atTick;
        }

        public override string ToString()
        {
            return "[MIDIMetaEvent GetEventType()=" + GetEventType() + "]";
        }

    }
}
