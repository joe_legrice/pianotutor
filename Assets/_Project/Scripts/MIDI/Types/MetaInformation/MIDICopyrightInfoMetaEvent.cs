﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDICopyrightInfoMetaEvent : MIDIMetaEvent
    {
        public string CopyrightInfo { get; private set; }

        public MIDICopyrightInfoMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            CopyrightInfo = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.CopyrightInfo;
        }

        public override string ToString()
        {
            return "[MIDICopyrightInfoMetaEvent CopyrightInfo=" + CopyrightInfo + "]";
        }
    }
}
