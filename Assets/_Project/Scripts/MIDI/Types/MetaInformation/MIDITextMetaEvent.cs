﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDITextMetaEvent : MIDIMetaEvent
    {
        public string Text { get; private set; }

        public MIDITextMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            Text = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.Text;
        }

        public override string ToString()
        {
            return "[MIDITextMetaEvent Text=" + Text + "]";
        }
    }
}
