﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDICueMetaEvent : MIDIMetaEvent
    {
        public string Text { get; private set; }

        public MIDICueMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            Text = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.Cue;
        }

        public override string ToString()
        {
            return "[MIDICueMetaEvent Text=" + Text + "]";
        }
    }
}
