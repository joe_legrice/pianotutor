﻿using System.Text;

namespace UnityMidiKit
{
    public class MIDIMarkerMetaEvent : MIDIMetaEvent
    {
        public string MarkerText { get; private set; }

        public MIDIMarkerMetaEvent(int atTick, byte[] data) : base(atTick, data)
        {
            MarkerText = Encoding.ASCII.GetString(data);
        }

        public override MIDIMetaEventType GetEventType()
        {
            return MIDIMetaEventType.Marker;
        }

        public override string ToString()
        {
            return "[MIDIMarkerMetaEvent MarkerText=" + MarkerText + "]";
        }
    }
}
