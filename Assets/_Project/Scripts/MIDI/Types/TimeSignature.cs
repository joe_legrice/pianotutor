﻿namespace UnityMidiKit
{
    public class TimeSignature
    {
        public int Numerator { get; private set; }
        public int Denominator { get; private set; }

        public TimeSignature(int numerator, int denominator)
        {
            Numerator = numerator;
            Denominator = denominator;
        }

        public override string ToString()
        {
            return Numerator + "/" + Denominator;
        }
    }
}
