﻿
namespace UnityMidiKit
{
    public abstract class MIDIMessage
    {
        public int Tick { get; private set; }
        public abstract MIDIEventType GetEventType();

        public MIDIMessage(int atTick, byte commandByte, byte[] dataBytes)
        {
            Tick = atTick;
        }
    }
}
