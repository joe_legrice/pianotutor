﻿using System;

namespace UnityMidiKit
{
    public enum MIDIEventType
    {
        Unknown = 0x00,
        NoteOff = 0x80,
        NoteOn = 0x90,
        PolyphonicKeyPressure = 0xa0,
        ControlChangeOrChannelMode = 0xb0,
        ProgramChange = 0xc0,
        ChannelPressure = 0xd0,
        PitchBendChange = 0xe0,

        // System Common
        SystemMessage = 0xf0,
        TimeCodeQuarterFrame = 0xf1,
        SongPositionPointer = 0xf2,
        SongSelect = 0xf3,
        TuneRequest = 0xf6,
        EndOfExclusive = 0xf7,

        // System Real-Time
        TimingClock = 0xf8,
        StartSequence = 0xfa,
        ContinueSequence = 0xfb,
        StopSequence = 0xfc,
        ActiveSensing = 0xfe,
        DeviceReset = 0xff
    }

    public enum MIDIMetaEventType
    {
        SetSequenceNumber = 0x00,
        Text = 0x01,
        CopyrightInfo = 0x02,
        TrackName = 0x03,
        Instrument = 0x04,
        Lyrics = 0x05,
        Marker = 0x06,
        Cue = 0x07,

        ChannelPrefix = 0x20,
        EndOfTrack = 0x2f,
        SetTempo = 0x51,
        SMPTEOffset = 0x54,
        TimeSignature = 0x58,
        KeySignature = 0x59,
        SequencerInfo = 0x7f,

        Unknown = 0xff
    }
    
    public enum ControlModeType
    {
        Unknown = 0xFF,

        BankSelect = 0x00,
        ModulationWheel = 0x01,
        BreathContoller = 0x02,
        FootController = 0x04,
        PortamentoTime = 0x05,
        DataEntryMSB = 0x06,
        MainVolume = 0x07,
        Balance = 0x08,
        PanMSB = 0x0a,
        ExpressionMSB = 0x0b,
        EffectControl1MSB = 0x0c,
        EffectControl2MSB = 0x0d,

        GeneralPurposeController1MSB = 0x10,
        GeneralPurposeController2MSB = 0x11,
        GeneralPurposeController3MSB = 0x12,
        GeneralPurposeController4MSB = 0x13,

        SustainPedal = 0x40, // [Data Byte of 0-63=0ff, 64-127=On]
        Portamento = 0x41,
        Sostenuto = 0x42,
        SoftPedal = 0x43,
        LegatoFootswitch = 0x44,
        Hold2 = 0x45,
        SoundController1 = 0x46, // default: Sound Variation
        SoundController2 = 0x47, // default: Timbre/Harmonic Content
        SoundController3 = 0x48, // default: Release Time
        SoundController4 = 0x49, // default: Attack Time
        SoundController5 = 0x4a, // default: Brightness
        SoundController6 = 0x4b,
        SoundController7 = 0x4c,
        SoundController8 = 0x4d,
        SoundController9 = 0x4e,
        SoundController10 = 0x4f,
        GeneralPurposeController1LSB = 0x50,
        GeneralPurposeController2LSB = 0x51,
        GeneralPurposeController3LSB = 0x52,
        GeneralPurposeController4LSB = 0x53,
        PortamentoControl = 0x54,
        EffectsDepthReverb = 0x5b,
        EffectsDepthTremolo = 0x5c,
        EffectsDepthChorus = 0x5d,
        EffectsDepthDetune = 0x5e,
        EffectsDepthPhaser = 0x5f,
        DataIncrement = 0x60,
        DataDecrement = 0x61,
        NonRegisteredParameterNumberLSB = 0x62,
        NonRegisteredParameterNumberMSB = 0x63,
        RegisteredParameterNumberLSB = 0x64,
        RegisteredParameterNumberMSB = 0x65,

        // Channel Mode Stuff
        ChannelMode = 0x70,
        AllSoundOff = 0x78,
        ResetAllControllers = 0x79,
        LocalControl = 0x7a,
        AllNotesOff = 0x7b,
        OmniModeOff = 0x7c,
        OmniModeOn = 0x7d,
        MonoModeOn = 0x7e,
        PolyModeOn = 0x7f
    }

    public static class MIDIEventTypeExtensions
    {
        public static MIDIMetaEvent GetClassInstance(this MIDIMetaEventType eventType, int atTick, byte[] data)
        {
            switch (eventType)
            {
                case MIDIMetaEventType.Cue:
                    return new MIDICueMetaEvent(atTick, data);
                case MIDIMetaEventType.Text:
                    return new MIDITextMetaEvent(atTick, data);
                case MIDIMetaEventType.Lyrics:
                    return new MIDILyricMetaEvent(atTick, data);
                case MIDIMetaEventType.Marker:
                    return new MIDIMarkerMetaEvent(atTick, data);
                case MIDIMetaEventType.TrackName:
                    return new MIDITrackNameMetaEvent(atTick, data);
                case MIDIMetaEventType.Instrument:
                    return new MIDIInstrumentMetaEvent(atTick, data);
                case MIDIMetaEventType.CopyrightInfo:
                    return new MIDICopyrightInfoMetaEvent(atTick, data);
                case MIDIMetaEventType.SetTempo:
                    return new MIDISetTempoMetaEvent(atTick, data);
                case MIDIMetaEventType.TimeSignature:
                    return new MIDITimeSignatureMetaEvent(atTick, data);
                default:
                    return null;
            }
        }

        public static MIDIMessage GetClassInstance(this MIDIEventType eventType, int atTick, byte commandByte, byte[] data)
        {
            switch (eventType)
            {
                case MIDIEventType.NoteOn:
                    if (data[1] != 0)
                    {
                        return new MIDINoteOnMessage(atTick, commandByte, data);
                    }
                    else
                    {
                        return new MIDINoteOffMessage(atTick, commandByte, data);
                    }
                case MIDIEventType.NoteOff:
                    return new MIDINoteOffMessage(atTick, commandByte, data);
                case MIDIEventType.ControlChangeOrChannelMode:
                    return new MIDIControlChangeMessage(atTick, commandByte, data);
                default:
                    return null;
            }
        }
    }
}
