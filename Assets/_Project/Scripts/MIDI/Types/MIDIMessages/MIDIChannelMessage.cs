﻿using UnityEngine;
using System.Collections;
using System;

namespace UnityMidiKit
{
    public abstract class MIDIChannelMessage : MIDIMessage
    {
        public int Channel { get; private set;  }

        public MIDIChannelMessage(int atTick, byte commandByte, byte[] databytes) : base(atTick, commandByte, databytes)
        {
            Channel = commandByte & 0x0f;
        }
    }
}
