﻿
namespace UnityMidiKit
{
    public class MIDINoteOffMessage : MIDIChannelMessage
    {
        public Pitch Pitch { get; private set; }

        public override MIDIEventType GetEventType()
        {
            return MIDIEventType.NoteOff;
        }

        public MIDINoteOffMessage(int atTick, byte commandByte, byte[] data) : base(atTick, commandByte, data)
        {
            Pitch = (Pitch)data[0];
        }

        public override string ToString()
        {
            return "[MIDINoteOffMessage Channel=" + Channel + ", Pitch =" + Pitch + "]";
        }
    }
}
