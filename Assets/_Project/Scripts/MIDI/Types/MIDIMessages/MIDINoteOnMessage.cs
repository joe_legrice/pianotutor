﻿
namespace UnityMidiKit
{
    public class MIDINoteOnMessage : MIDIChannelMessage
    {
        public Pitch Pitch { get; private set; }
        public int Velocity { get; private set; }

        public override MIDIEventType GetEventType()
        {
            return MIDIEventType.NoteOn;
        }

        public MIDINoteOnMessage(int atTick, byte commandByte, byte[] data) : base(atTick, commandByte, data)
        {
            Pitch = (Pitch)data[0];
            Velocity = data[1];
        }

        public override string ToString()
        {
            return "[MIDINoteOnMessage Channel=" + Channel + ", Pitch="+Pitch+", Velocity="+Velocity+"]";
        }
    }
}
