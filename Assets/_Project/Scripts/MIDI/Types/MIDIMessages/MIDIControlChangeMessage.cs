﻿using System;

namespace UnityMidiKit
{
    public class MIDIControlChangeMessage : MIDIChannelMessage
    {
        public ControlModeType ControlChangeMessageType { get; set; }

        public MIDIControlChangeMessage(int atTick, byte commandByte, byte[] databytes) : base(atTick, commandByte, databytes)
        {
            ControlModeType modeType = ControlModeType.Unknown;
            if (Enum.IsDefined(typeof(ControlModeType), (int)databytes[0]))
            {
                modeType = (ControlModeType)databytes[0];
            }
            ControlChangeMessageType = modeType;
        }

        public override MIDIEventType GetEventType()
        {
            return MIDIEventType.ControlChangeOrChannelMode;
        }

        public override string ToString()
        {
            return "[MIDIControlChangeMessage ControlChangeMessageType=" + ControlChangeMessageType + "]";
        }
    }
}
