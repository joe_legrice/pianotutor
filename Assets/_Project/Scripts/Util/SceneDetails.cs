﻿using UnityEngine;

[RequireComponent(typeof(tk2dCamera))]
public class SceneDetails : Singleton<SceneDetails>
{
    private static tk2dCamera m_camera;

    public static Camera SceneCamera { get { return m_camera.ScreenCamera; } }
    public static Vector3 MaxWorld { get; private set; }
    public static Vector3 MinWorld { get; private set; }
    public static Vector3 WorldSize { get; private set; }

    private void Awake()
    {
        m_camera = GetComponent<tk2dCamera>();
        MinWorld = SceneCamera.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f));
        MaxWorld = SceneCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
        WorldSize = MaxWorld - MinWorld;
    }

    public static bool IsReallyOnScreen(Collider c)
    {
        if (c != null)
        {
            return c.bounds.center.x + c.bounds.extents.x > MinWorld.x &&
                c.bounds.center.x - c.bounds.extents.x < MaxWorld.x &&
                c.bounds.center.y + c.bounds.extents.y > MinWorld.y &&
                c.bounds.center.y - c.bounds.extents.y < MaxWorld.y;
        }
        else
        {
            return false;
        }
    }
}
