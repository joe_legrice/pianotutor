﻿using UnityEngine;

public class AssetPathAttribute : PropertyAttribute
{
    public string RootPath { get; private set; }
    public string FileExtension { get; private set; }

    public AssetPathAttribute()
    {
        RootPath = "";
        FileExtension = "";
    }
    
    public AssetPathAttribute(string rootPath, string fileExtension)
    {
        RootPath = rootPath;
        FileExtension = fileExtension;
    }
}
