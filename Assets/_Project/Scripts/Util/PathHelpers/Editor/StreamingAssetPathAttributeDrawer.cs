﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(StreamingAssetPathAttribute))]
public class StreamingAssetPathAttributeDrawer : PropertyDrawer
{
    private StreamingAssetPathControl m_pathControl = new StreamingAssetPathControl();

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        m_pathControl.Path = property.stringValue;
        property.stringValue = m_pathControl.Draw(position);
        EditorGUI.EndProperty();
    }
}
