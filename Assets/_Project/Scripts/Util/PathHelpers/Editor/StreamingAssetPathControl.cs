﻿using UnityEngine;
using UnityEditor;

using System.IO;
using System.Linq;
using System.Collections.Generic;

public class StreamingAssetPathControl
{
    private int m_pathIndex;
    private string[] m_cachedDirectories;

    private string m_path;
    public string Path
    {
        get { return m_path; }
        set
        {
            if (m_path != value)
            {
                RefreshCache();
                for (int pathIndex = 0; pathIndex < m_cachedDirectories.Length; pathIndex++)
                {
                    if (m_cachedDirectories[pathIndex] == value)
                    {
                        m_path = value;
                        m_pathIndex = pathIndex;
                        break;
                    }
                }
            }
        }
    }

    public StreamingAssetPathControl() : this("")
    {
    }

    public StreamingAssetPathControl(string path)
    {
        Path = path;
        RefreshCache();
    }

    public string Draw()
    {
        m_pathIndex = EditorGUILayout.Popup(m_pathIndex, m_cachedDirectories);
        Path = m_cachedDirectories[m_pathIndex];
        return Path;
    }

    public string Draw(Rect r)
    {
        m_pathIndex = EditorGUI.Popup(r, m_pathIndex, m_cachedDirectories);
        Path = m_cachedDirectories[m_pathIndex];
        return Path;
    }

    public void RefreshCache()
    {
        List<string> files = new List<string>();
        ProcessDirectory(new DirectoryInfo(Application.streamingAssetsPath), files);
        m_cachedDirectories = files.ToArray();
    }

    private void ProcessDirectory(DirectoryInfo d, List<string> fileList)
    {
        fileList.AddRange(d.GetFiles().Select(fi => {
#if UNITY_EDITOR_WIN
            string streamingAssetPath = Application.streamingAssetsPath.Replace('/', '\\') + "\\";
#elif UNITY_EDITOR_OSX
            string streamingAssetPath = Application.streamingAssetsPath + "/";
#endif
            return fi.FullName.Replace(streamingAssetPath, "");
        }).Where(fileName => {
            return !fileName.Contains(".meta");
        }).ToArray());
        foreach (DirectoryInfo di in d.GetDirectories())
        {
            ProcessDirectory(di, fileList);
        }
    }
}
