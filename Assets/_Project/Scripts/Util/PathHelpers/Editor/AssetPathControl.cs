﻿using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

public class AssetPathControl
{
    private int m_pathIndex;
    private string m_rootPath = "";
    private string m_fileExtension = "";
    private static string[] m_cachedDirectories;

    public string Path { get; set; }

    public string RootPath
    {
        get { return m_rootPath; }
        set
        {
            if (m_rootPath != value)
            {
                m_rootPath = value;
                RefreshCache();
            }
        }
    }

    public string FileExtension
    {
        get
        {
            return m_fileExtension;
        }
        set
        {
            if (m_fileExtension != value)
            {
                m_fileExtension = value;
                RefreshCache();
            }
        }
    }

    public AssetPathControl() : this("", "")
    {
    }

    public AssetPathControl(string fileExtension, string rootPath)
    {
        Path = "";
        m_pathIndex = -1;
        m_rootPath = rootPath;
        m_fileExtension = fileExtension;

        RefreshCache();
    }

    public void RefreshCache()
    {
        List<string> directoryList = GetRelativeContents(m_rootPath, m_fileExtension);
        m_cachedDirectories = directoryList.ToArray();
    }

    public string Draw()
    {
        PatchUpPathIndex();
        m_pathIndex = EditorGUILayout.Popup(m_pathIndex, m_cachedDirectories);
        Path = m_cachedDirectories[m_pathIndex];
        ValidatePath();
        return Path;
    }

    public string Draw(Rect r)
    {
        PatchUpPathIndex();
        m_pathIndex = EditorGUI.Popup(r, m_pathIndex, m_cachedDirectories);
        Path = m_cachedDirectories[m_pathIndex];
        ValidatePath();
        return Path;
    }

    private void PatchUpPathIndex()
    {
        if (m_pathIndex == -1)
        {
            m_pathIndex = 0;
            for (int index = 0; index < m_cachedDirectories.Count(); ++index)
            {
                if (Path == m_cachedDirectories[index] || Path + "." == m_cachedDirectories[index])
                {
                    m_pathIndex = index;
                }
            }
        }
    }

    private void ValidatePath()
    {
        int validatedDirectoryIndex = Path.IndexOf('.');
        if (validatedDirectoryIndex == Path.Length - 1)
        {
            // Trim any "." character off the end if we selected a directory, not a file.
            Path = Path.Substring(0, validatedDirectoryIndex);
        }
    }

    private List<string> GetRelativeContents(string path, string fileExtension)
    {
        const char subDirSeperator = '/';

        bool directoryExists = string.IsNullOrEmpty(path);
        List<string> directories = new List<string>();
        if (directoryExists)
        {
            directories.Add(".");
        }
        else
        {
            directories.Add(path + subDirSeperator + '.');
        }

        string currentPath = Application.dataPath + subDirSeperator + path;
        DirectoryInfo directoryInfo = new DirectoryInfo(currentPath);
        DirectoryInfo[] subdirectoryInfo = directoryInfo.GetDirectories();
        FileInfo[] subdirectoryContentInfo = directoryInfo.GetFiles("*"+fileExtension);

        for (int i = 0; i < subdirectoryInfo.Length; i++)
        {
            if (directoryExists)
            {
                List<string> subdirectoryContents = GetRelativeContents(subdirectoryInfo[i].Name, fileExtension);
                directories.AddRange(subdirectoryContents);
            }
            else
            {
                string subdirectoryPath = path + subDirSeperator + subdirectoryInfo[i].Name;
                List<string> subdirectoryContents = GetRelativeContents(subdirectoryPath, fileExtension);
                directories.AddRange(subdirectoryContents);
            }
        }

        for (int i = 0; i < subdirectoryContentInfo.Length; i++)
        {
            directories.Add(path + subDirSeperator + subdirectoryContentInfo[i].Name);
        }

        return directories;
    }
}