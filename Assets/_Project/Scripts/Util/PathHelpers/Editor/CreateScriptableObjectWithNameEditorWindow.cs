﻿using UnityEngine;
using UnityEditor;

using System;
using System.Reflection;
using System.Collections.Generic;

public class CreateScriptableObjectWithNameEditorWindow : EditorWindow
{
    private static int s_scriptableTypeIndex;
    private static AssetPathControl s_pathControl;

    private static Type[] s_scriptableTypes;
    private static string[] s_scriptableTypeNames;

    private string m_assetName = "";
    private string m_assetPath = "";

    [MenuItem("Assets/Create/Scriptable Object")]
    public static void ShowWindow()
    {
        s_scriptableTypeIndex = 0;

        List<Type> types = new List<Type>();
        List<string> typeNames = new List<string>();

        Assembly[] allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
        foreach (Assembly ass in allAssemblies)
        {
            Type[] assTypes = ass.GetTypes();
            foreach (Type t in assTypes)
            {
                bool acceptedNameSpace = true;
                string space = t.Namespace;
                if (space != null)
                {
                    acceptedNameSpace = !space.Contains("UnityEditor");
                }

                if (t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(ScriptableObject)) && acceptedNameSpace)
                {
                    string typeName = t.FullName;
                    types.Add(t);
                    typeNames.Add(typeName);
                }
            }
        }
        s_scriptableTypes = types.ToArray();
        s_scriptableTypeNames = typeNames.ToArray();

        s_pathControl = new AssetPathControl();
        EditorWindow.GetWindow<CreateScriptableObjectWithNameEditorWindow>();
    }

    public void OnGUI()
    {
        EditorGUILayout.LabelField("Create Scriptable Object");

        //TODO: Create a "Make Another" checkbox.

        s_scriptableTypeIndex = EditorGUILayout.Popup("Type", s_scriptableTypeIndex, s_scriptableTypeNames);
        m_assetName = EditorGUILayout.TextField("Asset Name", m_assetName);

        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.PrefixLabel("Asset Path");
            m_assetPath = s_pathControl.Draw();
        }
        EditorGUILayout.EndHorizontal();

        bool OK = GUILayout.Button("Create");
        if (OK && !string.IsNullOrEmpty(m_assetName))
        {
            Type typeSelected = s_scriptableTypes[s_scriptableTypeIndex];
            //                string typeNameSelected = s_scriptableTypeNames[s_scriptableTypeIndex];

            ScriptableObject obj = ScriptableObject.CreateInstance(typeSelected) as ScriptableObject;
            AssetDatabase.CreateAsset(obj, "Assets/" + m_assetPath + m_assetName + ".asset");
            Close();
        }
    }
}
