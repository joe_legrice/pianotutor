﻿using UnityEngine;
using UnityEditor;

using System.IO;
using System.Linq;
using System.Collections.Generic;

public class ResourcePathControl
{
    private int m_pathIndex;
    private string[] m_cachedDirectories;

    private string m_path;
    public string Path
    {
        get { return m_path; }
        set
        {
            if (m_path != value)
            {
                RefreshCache();
                for (int pathIndex = 0; pathIndex < m_cachedDirectories.Length; pathIndex++)
                {
                    if (m_cachedDirectories[pathIndex] == value)
                    {
                        m_path = value;
                        m_pathIndex = pathIndex;
                        break;
                    }
                }
            }
        }
    }

    public ResourcePathControl() : this("")
    {
    }

    public ResourcePathControl(string path)
    {
        Path = path;
        RefreshCache();
    }

    public string Draw()
    {
        m_pathIndex = EditorGUILayout.Popup(m_pathIndex, m_cachedDirectories);
        Path = m_cachedDirectories[m_pathIndex];
        return Path;
    }

    public string Draw(Rect r)
    {
        m_pathIndex = EditorGUI.Popup(r, m_pathIndex, m_cachedDirectories);
        Path = m_cachedDirectories[m_pathIndex];
        return Path;
    }

    public void RefreshCache()
    {
        List<string> files = new List<string>();
        ProcessDirectory(new DirectoryInfo(Application.dataPath), files);
        m_cachedDirectories = files.ToArray();
    }

    private void ProcessDirectory(DirectoryInfo dirInfo, List<string> fileList)
    {
        if (dirInfo.Name == "Resources")
        {
            AddResourceDirectory(dirInfo, fileList);
        }
        else
        {
            DirectoryInfo[] subdirs = dirInfo.GetDirectories();
            foreach (DirectoryInfo di in subdirs)
            {
                ProcessDirectory(di, fileList);
            }
        }
    }

    private void AddResourceDirectory(DirectoryInfo d, List<string> fileList)
    {
        fileList.AddRange(d.GetFiles().Select(fi => {
            string searchString = "Resources\\";
            int i = fi.FullName.IndexOf(searchString) + searchString.Length;
            return fi.FullName.Substring(i, fi.FullName.Length - i);
        }).Where(fileName => {
            return !fileName.Contains(".meta");
        }).ToArray());
        foreach (DirectoryInfo di in d.GetDirectories())
        {
            AddResourceDirectory(di, fileList);
        }
    }
}
