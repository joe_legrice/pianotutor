﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(AssetPathAttribute))]
public class AssetPathAttributeDrawer : PropertyDrawer
{
    private AssetPathControl m_pathControl = new AssetPathControl();

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        m_pathControl.Path = property.stringValue;
        m_pathControl.RootPath = ((AssetPathAttribute)attribute).RootPath;
        m_pathControl.FileExtension = ((AssetPathAttribute)attribute).FileExtension;
        property.stringValue = m_pathControl.Draw(position);
        EditorGUI.EndProperty();
    }
}
