﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ResourcePathAttribute))]
public class ResourcePathAttributeDrawer : PropertyDrawer
{
    private ResourcePathControl m_pathControl = new ResourcePathControl();

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        m_pathControl.Path = property.stringValue;
        property.stringValue = m_pathControl.Draw(position);
        EditorGUI.EndProperty();
    }
}
