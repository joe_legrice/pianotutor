﻿using UnityEngine;
using System.Collections;

public class HierarchyController : MonoBehaviour
{
    [SerializeField] private GameObject m_activeHierarchy;
    [SerializeField] private GameObject m_inactiveHierarchy;
    [SerializeField] private bool m_initialState;

    private bool m_currentState;

    private void Awake()
    {
        m_currentState = !m_initialState;
        SetState(m_initialState);
    }

    public void SetState(bool state)
    {
        if (m_currentState != state)
        {
            if (m_activeHierarchy != null)
            {
                m_activeHierarchy.SetActive(state);
            }

            if (m_inactiveHierarchy != null)
            {
                m_inactiveHierarchy.SetActive(!state);
            }

            m_currentState = state;
        }
    }
}
