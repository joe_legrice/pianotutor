﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static readonly object s_lock = new object();
    private static T s_instance;
    public static T Instance
    {
        get
        {
            if (s_instance == null)
            {
                lock (s_lock)
                {
                    s_instance = FindObjectOfType<T>();
                }
                if (s_instance == null)
                {
                    GameObject instance = new GameObject();
                    s_instance = instance.AddComponent<T>();
                    s_instance.name = typeof(T).Name + "_Singleton";
                }
            }
            return s_instance;
        }
    }
}
