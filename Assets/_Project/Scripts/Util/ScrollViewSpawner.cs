﻿using UnityEngine;
using System.Collections.Generic;

public class ScrollViewSpawner : MonoBehaviour
{
    [SerializeField] private GameObject m_scrollViewItemPrefab;
    [SerializeField] private tk2dUIScrollableArea m_scrollArea;
    [SerializeField] private LayoutController m_layoutControl;

    private List<GameObject> m_objects = new List<GameObject>();

    public List<GameObject> Objects { get { return m_objects.GetRange(0, m_objects.Count); } }

    public List<T> GetComponentInObjects<T>()
    {
        List<T> result = new List<T>();
        foreach (GameObject go in m_objects)
        {
            result.Add(go.GetComponent<T>());
        }
        return result;
    }

    public void RefreshItems(int number)
    {
        ClearAllObjects();

        Transform parentTransform = m_layoutControl.GetComponent<Transform>();
        for (int i = 0; i < number; i++)
        {
            GameObject instance = Instantiate(m_scrollViewItemPrefab);
            instance.GetComponent<Transform>().parent = parentTransform;
            m_objects.Add(instance);
        }
        m_layoutControl.RepositionChildren();
        m_scrollArea.ContentLength = m_scrollArea.MeasureContentLength();
    }

    public void ClearAllObjects()
    {
        foreach (GameObject go in m_objects)
        {
            DestroyImmediate(go);
        }
        m_objects.Clear();
    }
}
