﻿using UnityEngine;
using System.Collections;

public class CancelConfiguration : MonoBehaviour
{
    [SerializeField] private tk2dUIItem m_uiItem;
    [SerializeField] private ConfigureCurrentDevice m_configurationScreen;

    private void Awake()
    {
        m_uiItem.OnClick += HandleClick;
    }

    private void HandleClick()
    {
        m_configurationScreen.CancelConfiguration();
    }
}
