﻿using UnityMidiKit;
using Newtonsoft.Json;

[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
public class DeviceConfiguration : ISaveSerializer
{
    [JsonProperty] public int? DeviceID { get; set; }
    [JsonProperty] public Pitch? MinPitch { get; set; }
    [JsonProperty] public Pitch? MaxPitch { get; set; }
    
    public string SerializeToString()
    {
        return JsonConvert.SerializeObject(this);
    }

    public void RestoreFromString(string json)
    {
        JsonConvert.PopulateObject(json, this);
    }
}
