﻿using UnityEngine;
using UnityMidiKit;
using System.Collections;

public class ConfigureCurrentDevice : MonoBehaviour, ISourceListener
{
    [SerializeField] private GameObject m_commonHierarchy;
    [SerializeField] private GameObject m_pressFirstKeyHierarchy;
    [SerializeField] private GameObject m_pressLastKeyHierarchy;
    [SerializeField] private GameObject m_setupSuccessHierarchy;
    [SerializeField] private GameObject m_setupFailureHierarchy;
    [SerializeField] private float m_screenShowDelay = 3.0f;

    private enum SetupState
    {
        None,
        PressFirstKey,
        PressLastKey,
        ValidateKeys,
        Failure,
        Success
    }

    private SetupState m_currentState;
    private DeviceConfiguration m_deviceConfig;
    private static readonly string s_defaultConfigKey = "defaultDeviceConfiguration";

    
    public void StartConfiguration()
    {
        StartCoroutine(ReconfigureCurrentDevice());
    }

    public void CancelConfiguration()
    {
        if (m_currentState != SetupState.None)
        {
            StopAllCoroutines();
            m_deviceConfig = null;
            m_currentState = SetupState.None;
            DeviceManager.Instance.DisconnectSource();
        }
    }

    private void Awake()
    {
        DeviceManager.Instance.RegisterDeviceListener(this);
        m_currentState = SetupState.None;
    }

    private void Start()
    {
        // Try to load default configuration
        if (DeviceManager.Instance.CurrentSource == null &&
            SaveDataManager.Instance.SaveDataExistsForKey(s_defaultConfigKey))
        {
            string data = SaveDataManager.Instance.GetStringForKey(s_defaultConfigKey);
            if (!string.IsNullOrEmpty(data))
            {
                m_deviceConfig = new DeviceConfiguration();
                m_deviceConfig.RestoreFromString(data);
            }
        }
    }

    private void Update()
    {
        m_commonHierarchy.SetActive(m_currentState != SetupState.None);
        m_pressFirstKeyHierarchy.SetActive(m_currentState == SetupState.PressFirstKey);
        m_pressLastKeyHierarchy.SetActive(m_currentState == SetupState.PressLastKey);
        m_setupFailureHierarchy.SetActive(m_currentState == SetupState.Failure);
        m_setupSuccessHierarchy.SetActive(m_currentState == SetupState.Success);
    }

    public void OnSourceConnected(MIDISourceEntity se)
    {
        se.OnNotePressed += HandleNotePress;
    }

    public void OnSourceDisconnected(MIDISourceEntity se)
    {
        se.OnNotePressed -= HandleNotePress;
    }

    private IEnumerator ReconfigureCurrentDevice()
    {
        m_currentState = SetupState.PressFirstKey;
        while (m_currentState != SetupState.Success)
        {
            m_deviceConfig = new DeviceConfiguration();
            m_deviceConfig.DeviceID = DeviceManager.Instance.CurrentSource.EntityID;

            while (m_currentState != SetupState.ValidateKeys)
            {
                yield return null;
            }
            
            if (IsValidRange(m_deviceConfig.MinPitch.Value, m_deviceConfig.MaxPitch.Value))
            {
                m_currentState = SetupState.Success;
            }
            else
            {
                m_currentState = SetupState.Failure;
                yield return new WaitForSeconds(m_screenShowDelay);
                m_currentState = SetupState.PressFirstKey;
            }
        }
        
        yield return new WaitForSeconds(m_screenShowDelay);
        m_currentState = SetupState.None;
        
        // Save default config
        SaveDataManager.Instance.SaveObjectForKey(s_defaultConfigKey, m_deviceConfig);
    }

    private bool IsValidRange(Pitch minPitch, Pitch maxPitch)
    {
        return !minPitch.IsSharp() && !maxPitch.IsSharp() && minPitch >= 0 && maxPitch > minPitch;
    }

    private void HandleNotePress(MIDINoteOnMessage msg)
    {
        if (m_currentState == SetupState.PressFirstKey)
        {
            m_deviceConfig.MinPitch = msg.Pitch;
            m_currentState = SetupState.PressLastKey;
        }
        else if (m_currentState == SetupState.PressLastKey)
        {
            m_deviceConfig.MaxPitch = msg.Pitch;
            m_currentState = SetupState.ValidateKeys;
        }
    }
}
