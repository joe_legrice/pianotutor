﻿using UnityEngine;
using UnityMidiKit;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class MIDIConfigurationScreen : MonoBehaviour
{
    [SerializeField] private DropdownMenu m_dropdownMenu;

    private List<MIDIDevice> m_deviceOutputs;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f); // lol
        m_deviceOutputs = new List<MIDIDevice>();
        foreach (MIDIDevice device in MIDI.AllDevices)
        {
            if (device.DeviceOutputs.Count > 0)
            {
                m_deviceOutputs.Add(device);
            }
        }

        List<string> names = m_deviceOutputs.Select(od => od.Name).ToList();
        names.Insert(0, "None Selected");

        m_dropdownMenu.Setup(names, 0, ToggleDevice);
    }

    private bool ToggleDevice(int midiDeviceIndex)
    {
        if (midiDeviceIndex > 0)
        {
            return DeviceManager.Instance.SelectSource(m_deviceOutputs[midiDeviceIndex - 1].DeviceOutputs[0]);
        }
        else if (midiDeviceIndex == 0)
        {
            return DeviceManager.Instance.DisconnectSource();
        }
        else
        {
            return false;
        }
    }
}
